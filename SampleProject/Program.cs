﻿using System;
using Nimbus;

namespace Template {
	class CharacterController : NimbusScript {
		public override void Update() {

		}
	}

	class MainClass {
		public static void Main(string[] args) {
			Engine engine = new Engine("template.xml");
			engine.MainWindow.CloseEvent += (s, e) => Environment.Exit(0);
			engine.LoadLevel("level1");
			engine.MainLoop();
		}
	}
}
