// Distributed as part of TiledSharp, Copyright 2012 Marshall Ward
// Licensed under the Apache License, Version 2.0
// http://www.apache.org/licenses/LICENSE-2.0
using System;
using System.Xml.Linq;

namespace TiledSharp
{
    public class TmxImageLayer : ITmxElement
    {
        public string Name {get; private set;}
		public int X {get; private set;}
		public int Y {get; private set;}
        public int Width {get; private set;}
        public int Height {get; private set;}

        public bool Visible {get; private set;}
        public double Opacity {get; private set;}

        public TmxImage Image {get; private set;}

        public PropertyDict Properties {get; private set;}

        public TmxImageLayer(XElement xImageLayer, string tmxDir = "")
        {
            Name = (string)xImageLayer.Attribute("name");

			X = (int?)xImageLayer.Attribute("x") ?? 0;
			Y = (int?)xImageLayer.Attribute("y") ?? 0;
			Width = (int?)xImageLayer.Attribute("width") ?? 0;
			Height = (int?)xImageLayer.Attribute("height") ?? 0;
            Visible = (bool?)xImageLayer.Attribute("visible") ?? true;
            Opacity = (double?)xImageLayer.Attribute("opacity") ?? 1.0;

            Image = new TmxImage(xImageLayer.Element("image"), tmxDir);

            Properties = new PropertyDict(xImageLayer.Element("properties"));
        }
    }
}
