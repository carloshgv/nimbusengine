﻿using System;
using System.Diagnostics;

namespace Nimbus {
	public class Time {
		public static float deltaT;
		public static float time;

		static float fixedDeltaT;
		static float realDeltaT;
		static float remainingDeltaT;
		static float maxDeltaT = 1 / 30f;
		static Stopwatch stopwatch;

		internal static void InitTimer() {
			stopwatch = Stopwatch.StartNew();
			time = 0;
		}

		internal static void UpdateTime() {
			stopwatch.Stop();
			realDeltaT = (float)stopwatch.ElapsedTicks / Stopwatch.Frequency;
			time += realDeltaT;
			remainingDeltaT = realDeltaT;
			fixedDeltaT = Math.Min(realDeltaT, maxDeltaT);
			stopwatch.Restart();
		}

		internal static bool consumeTime() {
			remainingDeltaT -= fixedDeltaT;
			fixedDeltaT = Math.Min(realDeltaT, maxDeltaT);
			return remainingDeltaT > 0.0f;
		}

		internal static void UseFixedDeltaT() {
			deltaT = fixedDeltaT;
		}

		internal static void UseRealDeltaT() {
			deltaT = realDeltaT;
		}
	}
}

