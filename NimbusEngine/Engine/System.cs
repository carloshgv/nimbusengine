﻿using System;
using System.Diagnostics;
using System.Collections.Generic;

namespace Nimbus {
	public abstract class System {
		public Engine owner;
		internal List<Entity> targets;

		public virtual void Init() { }

		public virtual void FramePrefix() {
			throw new ApplicationException("nooo!!!!");
		}

		public virtual void PreUpdate(Entity entity) {
			throw new ApplicationException("nooo!!!!");
		}

		public virtual void PreFixedUpdate(Entity entity) {
			throw new ApplicationException("nooo!!!!");
		}

		public virtual void FixedUpdate(Entity entity) {
			throw new ApplicationException("nooo!!!!");
		}

		public virtual void Update(Entity entity) {
			throw new ApplicationException("nooo!!!!");
		}

		public virtual void LateUpdate(Entity entity) {
			throw new ApplicationException("nooo!!!!");
		}

		public virtual void FrameSuffix() {
			throw new ApplicationException("nooo!!!!");
		}

		public virtual void Render(Entity entity) {
			throw new ApplicationException("nooo!!!!");
		}

		public virtual void LateRender(Entity entity) {
			throw new ApplicationException("nooo!!!!");
		}

		public virtual void OnDestroy() {

		}

		[Conditional("RENDER_DEBUG")]
		public virtual void RenderDebug(Entity entity) {
			throw new ApplicationException("nooo!!!!");
		}

		protected T GetSystem<T>() {
			return owner.GetSystem<T>();
		}

		protected Entity CreateEntity(params Component[] comps) {
			return owner.CreateEntity(comps);
			;
		}

		protected Entity CreateEntity(string tag, params Component[] comps) {
			return owner.CreateEntity(tag, comps);
		}

		protected Entity CreateEntity(LayerMask mask, params Component[] comps) {
			return owner.CreateEntity(mask, comps);
		}

		protected Entity CreateEntity(string tag, LayerMask mask, params Component[] comps) {
			return owner.CreateEntity(tag, mask, comps);
		}

		protected Entity CreateEntityFromTemplate(string id) {
			return owner.CreateEntityFromTemplate(id);
		}

		protected void DestroyEntity(Entity e) {
			owner.DestroyEntity(e);
		}

		protected List<Entity> FindEntitiesByTag(string tag) {
			return owner.FindEntitiesByTag(tag);
		}

		protected Entity FindEntityByTag(string tag) {
			return owner.FindEntityByTag(tag);
		}

		protected List<Entity> GetEntitiesWithComponent<T>() {
			return owner.GetEntitiesWithComponent<T>();
		}

		protected  void LoadLevel(string level) {
			owner.LoadLevel(level);
		}

		protected  void ReplayLevel() {
			owner.ReplayLevel();
		}

		public Backend.Window MainWindow { get { return owner.MainWindow; } }

		public AssetManager AssetManager { get { return owner.AssetManager; } }
	}
}
