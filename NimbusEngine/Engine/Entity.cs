﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nimbus.Backend;
using SDL2;
using System.Diagnostics;

namespace Nimbus {
	public sealed class Entity {
		internal Engine owner;

		public int id;
		public string tag;
		public LayerMask layerMask;

		private RigidBody rigidbodyCache;
		private Transform transformCache;
		private List<Animation> animationCache;
		private List<Collider> colliderCache;

		public RigidBody rigidbody {
			get {
				if(rigidbodyCache == null)
					rigidbodyCache = GetComponent<RigidBody>();
				return rigidbodyCache;
			}
		}

		public Transform transform {
			get {
				if(transformCache == null)
					transformCache = GetComponent<Transform>();
				return transformCache;
			}
		}

		public Animation animation { get { return (animations != null) ? animations[0] : null; } }

		public Collider collider { get { return (colliders != null) ? colliders[0] : null; } }

		public List<Animation> animations { 
			get {
				if(animationCache == null)
					animationCache = GetComponents<Animation>();
				return animationCache; 
			} 
		}

		public List<Collider> colliders { 
			get {
				if(colliderCache == null)
					colliderCache = GetComponents<Collider>();
				return colliderCache; 
			} 
		}

		internal Entity(int id, Engine owner) {
			this.id = id;
			this.owner = owner;
		}

		public T GetComponent<T>() where T : Component {
			return (T)Convert.ChangeType(owner.GetComponent(id, typeof(T)), typeof(T));
		}

		public List<T> GetComponents<T>() where T : Component {
			var components = owner.GetComponents(id, typeof(T));
			if(components == null)
				return null;
			else
				return owner.GetComponents(id, typeof(T)).ConvertAll(c => (T)Convert.ChangeType(c, typeof(T)));
		}

		public Component AddComponent(Component component) {
			return owner.AddComponent(this, component);
		}

		public void RemoveComponent(Component component) {
			animationCache = null;
			rigidbodyCache = null;
			transformCache = null;
			colliderCache = null;
			owner.RemoveComponent(this, component);
		}

		public override int GetHashCode() {
			return id;
		}
	}
}

