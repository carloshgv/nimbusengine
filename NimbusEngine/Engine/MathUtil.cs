﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Nimbus {
	public static class MathUtil {
		public static OverlapInfo SATOverlap(Rectangle a, Rectangle b) {
			float interval, x, y;
			Vector2 separationAxis;

			float ax1 = Math.Min (a.x, a.x + a.w);
			float ax2 = Math.Max (a.x, a.x + a.w);
			float ay1 = Math.Min (a.y, a.y + a.h);
			float ay2 = Math.Max (a.y, a.y + a.h);
			float bx1 = Math.Min (b.x, b.x + b.w);
			float bx2 = Math.Max (b.x, b.x + b.w);
			float by1 = Math.Min (b.y, b.y + b.h);
			float by2 = Math.Max (b.y, b.y + b.h);

			float right = Math.Abs(ax1 - bx2);
			float left = Math.Abs(ax2 - bx1);
			float bottom = Math.Abs(ay1 - by2);
			float top = Math.Abs(ay2 - by1);

			if(left < right) {
				x = left;
				separationAxis.x = 1;
			} else {
				x = right;
				separationAxis.x = -1;
			}

			if(bottom < top) {
				y = bottom;
				separationAxis.y = -1;
			} else {
				y = top;
				separationAxis.y = 1;
			}

			if(Math.Abs(x) < y) {
				interval = x;
				separationAxis.y = 0;
			} else {
				interval = y;
				separationAxis.x = 0;
			}

			if(bx2 >= ax1 && bx1 <= ax2 && by1 <= ay2 && by2 >= ay1)
				return new OverlapInfo(
					interval: interval,
					separationAxis: separationAxis
				);
			return null;
		}

		public static OverlapInfo SATOverlap(Rectangle a, Polygon b) {
			return SATOverlap(new Polygon(a), b);
		}

		public static OverlapInfo SATOverlap(Polygon a, Rectangle b) {
			return SATOverlap(a, new Polygon(b));
		}

		public static OverlapInfo SATOverlap(Polygon one, Polygon other) {
			float interval = float.MaxValue;
			Vector2 separationAxis = Vector2.Zero;
			Vector2 centerOne = one.Center;
			Vector2 centerOther = other.Center;
			Vector2 distance = centerOne - centerOther;

			// loop one edges
			for(int i = 0; i < one.NumEdges; i++) {
				Segment edge = one.Edge(i);
				if(edge.start == edge.end)
					continue;
				Vector2 axis = one.Normal(i);

				float currentInterval = 0;
				var projectionsOne = one.points.Select(p => axis.Dot(p));
				var projectionsOther = other.points.Select(p => axis.Dot(p));
				float distance1 = projectionsOne.Min() - projectionsOther.Max();
				float distance2 = projectionsOther.Min() - projectionsOne.Max();
				if(distance1 > 0 || distance2 > 0)
					return null;
						
				currentInterval = Math.Abs(Math.Max(distance1, distance2));

				if(currentInterval < interval) {
					interval = currentInterval;
					separationAxis = axis;
					if(distance.Dot(axis) > 0)
						separationAxis = -axis;
				}
			}

			// loop other edges
			for(int i = 0; i < other.NumEdges; i++) {
				Segment edge = other.Edge(i);
				if(edge.start == edge.end)
					continue;
				Vector2 axis = other.Normal(i);

				float currentInterval = 0;
				var projectionsOne = one.points.Select(p => axis.Dot(p));
				var projectionsOther = other.points.Select(p => axis.Dot(p));
				float distance1 = projectionsOne.Min() - projectionsOther.Max();
				float distance2 = projectionsOther.Min() - projectionsOne.Max();
				if(distance1 > 0 || distance2 > 0)
					return null;

				currentInterval = Math.Abs(Math.Max(distance1, distance2));

				if(currentInterval < interval) {
					interval = currentInterval;
					separationAxis = axis;
					if(distance.Dot(axis) > 0)
						separationAxis = -axis;
				}
			}

			return new OverlapInfo(interval, separationAxis);
		}

		public static Vector2? Intersection(Segment a, Segment b) {
			float p0_x = a.start.x;
			float p0_y = a.start.y;
			float p1_x = a.end.x;
			float p1_y = a.end.y;
			float p2_x = b.start.x;
			float p2_y = b.start.y;
			float p3_x = b.end.x;
			float p3_y = b.end.y;

			float s1_x, s1_y, s2_x, s2_y;
			s1_x = p1_x - p0_x;
			s1_y = p1_y - p0_y;
			s2_x = p3_x - p2_x;
			s2_y = p3_y - p2_y;

			float s, t;
			s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y)) / (-s2_x * s1_y + s1_x * s2_y);
			t = (s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x)) / (-s2_x * s1_y + s1_x * s2_y);

			if(s >= 0 && s <= 1 && t >= 0 && t <= 1)
				return new Vector2(p0_x + (t * s1_x), p0_y + (t * s1_y));

			return null;
		}
	}
}

