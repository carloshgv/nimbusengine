﻿using System;

namespace Nimbus {
	public class CollisionInfo {
		public readonly Collider collider;
		public readonly Entity entity;
		public readonly Vector2 axis;
		public readonly float interval;

		public CollisionInfo(Collider hit, Vector2 axis, float interval) {
			this.collider = hit;
			this.entity = hit.entity;
			this.axis = axis;
			this.interval = interval;
		}
	}

	public class RayHit {
		public readonly Collider[] colliders;
		public readonly Entity[] entities;
		public readonly Vector2[] points;
		public readonly Vector2[] normals;

		public Collider collider { get { return colliders[0]; } }

		public Entity entity { get { return colliders[0].entity; } }

		public Vector2 point { get { return points[0]; } }

		public Vector2 normal { get { return normals[0]; } }

		public RayHit(Collider[] colliders, Vector2[] points, Vector2[] normals) {
			this.colliders = colliders;
			this.points = points;
			this.normals = normals;
		}
	}

	public class OverlapInfo {
		public readonly float interval;
		public readonly Vector2 separationAxis;

		public OverlapInfo(float interval, Vector2 separationAxis) {
			this.interval = interval;
			this.separationAxis = separationAxis;
		}

		public static implicit operator bool(OverlapInfo info) {
			return info != null;
		}
	}

	public struct LayerMask {
		public static readonly LayerMask All = new LayerMask(~((uint)0));
		public static readonly LayerMask None = new LayerMask(0);

		uint mask;

		private LayerMask(uint mask) {
			this.mask = mask;
		}

		public static LayerMask Create(params int[] layers) {
			uint mask = 0;
			foreach(int layer in layers)
				mask |= (uint)1 << layer;
			return new LayerMask(mask);
		}

		public void AddLayer(int layer) {
			mask |= (uint)(~(1 << layer));
		}

		public void RemoveLayer(int layer) {
			mask &= (uint)(~(1 << layer));
		}

		public static bool operator &(LayerMask l1, LayerMask l2) {
			return (l1.mask & l2.mask) != 0;
		}

		public static LayerMask operator +(LayerMask l1, LayerMask l2) {
			return new LayerMask(l1.mask | l2.mask);
		}
	}
}


