﻿using System;
using SDL2;

namespace Nimbus {
	public class Rectangle : ICollidable {
		public static readonly Rectangle Zero = new Rectangle(0f, 0f, 0f, 0f);
		public static readonly Rectangle Unit = new Rectangle(0f, 0f, 1f, 1f);

		public float x, y, w, h;

		public float x1 { get { return x; } }

		public float x2 { get { return x + w; } }

		public float y1 { get { return y; } }

		public float y2 { get { return y + h; } }

		public Vector2 p1 { get { return new Vector2(x, y); } }

		public Vector2 p2 { get { return new Vector2(x + w, y + h); } }

		public Vector2 Size { get { return new Vector2(w, h); } }

		public Rectangle BoundingBox {
			get { return new Rectangle(this); }
		}

		public Vector2 Center {
			get { return new Vector2(x + w / 2, y + h / 2); }
		}

		public Segment[] Edges {
			get {
				Segment[] ret = new Segment[4];
				ret[0] = new Segment(new Vector2(x1, y1), new Vector2(x2, y1));
				ret[1] = new Segment(new Vector2(x2, y1), new Vector2(x2, y2));
				ret[2] = new Segment(new Vector2(x2, y2), new Vector2(x1, y2));
				ret[3] = new Segment(new Vector2(x1, y2), new Vector2(x1, y1));
				return ret;
			}
		}

		public Rectangle(float x, float y, float w, float h) {
			this.x = x;
			this.y = y;
			this.w = w;
			this.h = h;
		}

		public Rectangle(Vector2 p, Vector2 s) {
			this.x = p.x;
			this.y = p.y;
			this.w = s.x;
			this.h = s.y;
		}

		public Rectangle(Rectangle other) : this(other.x, other.y, other.w, other.h) {
		}

		public static Rectangle operator +(Rectangle rect, Vector2 v) {
			return new Rectangle(rect.x + v.x, rect.y + v.y, rect.w, rect.h);
		}

		public static Rectangle operator -(Rectangle rect, Vector2 v) {
			return new Rectangle(rect.x - v.x, rect.y - v.y, rect.w, rect.h);
		}

		public static Rectangle operator +(Rectangle rect, SDL.SDL_Point v) {
			return new Rectangle(rect.x + v.x, rect.y + v.y, rect.w, rect.h);
		}

		public static Rectangle operator -(Rectangle rect, SDL.SDL_Point v) {
			return new Rectangle(rect.x - v.x, rect.y - v.y, rect.w, rect.h);
		}

		public static Rectangle operator *(Rectangle rect, Vector2 v) {
			return new Rectangle(rect.x * v.x, rect.y * v.y, rect.w * v.x, rect.h * v.y);
		}

		public static Rectangle operator /(Rectangle rect, Vector2 v) {
			return new Rectangle(rect.x * v.x, rect.y * v.y, rect.w / v.x, rect.h / v.y);
		}

		public static Rectangle operator *(Rectangle rect, float f) {
			return new Rectangle(rect.x * f, rect.y * f, rect.w * f, rect.h * f);
		}

		public static Rectangle operator /(Rectangle rect, float f) {
			return new Rectangle(rect.x / f, rect.y / f, rect.w / f, rect.h / f);
		}

		public static implicit operator SDL.SDL_Rect(Rectangle r) {
			return new SDL.SDL_Rect { x = (int)Math.Floor(r.x), y = (int)Math.Floor(r.y), w = (int)Math.Floor(r.w), h = (int)Math.Floor(r.h) };
		}

		public static explicit operator Rectangle(SDL.SDL_Rect r) {
			return new Rectangle(r.x, r.y, r.w, r.h);
		}

		public bool Contains(Rectangle other) {
			return x1 <= other.x1 && x2 >= other.x2 && y1 <= other.y1 && y2 >= other.y2;
		}

		public bool Contains(Vector2 point) {
			return x1 <= point.x && x2 >= point.x && y1 <= point.y && y2 >= point.y;
		}

		public override string ToString() {
			return "[" + x + ", " + y + ", " + w + ", " + h + "]";
		}

		public OverlapInfo Overlaps(IOverlapable other) {
			if(other.GetType() == typeof(Rectangle)) {
				return MathUtil.SATOverlap(this, (Rectangle)other);
			} else if(other.GetType() == typeof(Polygon)) {
				return MathUtil.SATOverlap(this, other as Polygon);
			} else {
				return Overlaps(other.BoundingBox);
			}
		}

		public ITransformable Transform(Transform t) {
			return this * t;
		}
	}
}

