﻿using System;
using SDL2;

namespace Nimbus {
	public struct Vector2 {
		public static readonly Vector2 CenterCenter = new Vector2(0.5f, 0.5f);
		public static readonly Vector2 CenterTop = new Vector2(0.5f, 0f);
		public static readonly Vector2 CenterBottom = new Vector2(0.5f, 1f);
		public static readonly Vector2 LeftCenter = new Vector2(0f, 0.5f);
		public static readonly Vector2 LeftTop = new Vector2(0f, 0f);
		public static readonly Vector2 LeftBottom = new Vector2(0f, 1f);
		public static readonly Vector2 RightCenter = new Vector2(1f, 0.5f);
		public static readonly Vector2 RightTop = new Vector2(1f, 0f);
		public static readonly Vector2 RightBottom = new Vector2(1f, 1f);

		public static readonly Vector2 Left = new Vector2(-1f, 0f);
		public static readonly Vector2 Right = new Vector2(1f, 0f);
		public static readonly Vector2 Up = new Vector2(0f, -1f);
		public static readonly Vector2 Down = new Vector2(0f, 1f);
		public static readonly Vector2 Zero = new Vector2(0f, 0f);
		public static readonly Vector2 Identity = new Vector2(1f, 1f);

		public float x, y;

		public Vector2(float x, float y) {
			this.x = x;
			this.y = y;
		}

		public Vector2(Vector2 other) {
			this.x = other.x;
			this.y = other.y;
		}

		public float SqrDistance(Vector2 other) {
			return (other.x - x) * (other.x - x) + (other.y - y) * (other.y - y);
		}

		public float Distance(Vector2 other) {
			return (float)Math.Sqrt(SqrDistance(other));
		}

		public float Length {
			get {
				return (float)Math.Sqrt(x * x + y * y);
			}
		}

		public Vector2 Normalized {
			get {
				float len = Length;
				if(len < float.Epsilon)
					throw new DivideByZeroException();
				return new Vector2(x / len, y / len);
			}
		}

		public Vector2 UnitNormal {
			get {
				return new Vector2(y, -x).Normalized;
			}
		}

		public Vector2 Normal {
			get {
				return new Vector2(y, -x);
			}
		}

		public float Dot(Vector2 v2) {
			return x * v2.x + y * v2.y;
		}

		public static Vector2 operator *(float f, Vector2 v) {
			return new Vector2(v.x * f, v.y * f);
		}

		public static Vector2 operator *(Vector2 v, float f) {
			return new Vector2(v.x * f, v.y * f);
		}

		public static Vector2 operator /(Vector2 v, float f) {
			return new Vector2(v.x / f, v.y / f);
		}

		public static Vector2 operator +(Vector2 v1, Vector2 v2) {
			return new Vector2(v1.x + v2.x, v1.y + v2.y);
		}

		public static Vector2 operator -(Vector2 v1, Vector2 v2) {
			return new Vector2(v1.x - v2.x, v1.y - v2.y);
		}

		public static Vector2 operator -(Vector2 v1) {
			return new Vector2(-v1.x, -v1.y);
		}

		public static Vector2 operator *(Vector2 v1, Vector2 v2) {
			return new Vector2(v1.x * v2.x, v1.y * v2.y);
		}

		public static Vector2 operator /(Vector2 v1, Vector2 v2) {
			return new Vector2(v1.x / v2.x, v1.y / v2.y);
		}

		public static Vector2 operator *(Vector2 v1, Rotation r) {
			float cos = (float)Math.Cos(r.Rad),
			sin = (float)Math.Sin(r.Rad);
			return new Vector2(v1.x * cos - v1.y * sin,
			                   v1.x * sin + v1.y * cos);
		}

		public static bool operator ==(Vector2 one, Vector2 other) {
			return one.Equals(other);
		}

		public static bool operator !=(Vector2 one, Vector2 other) {
			return !one.Equals(other);
		}

		public static implicit operator SDL.SDL_Point(Vector2 p) {
			return new SDL.SDL_Point { x = (int)Math.Floor(p.x), y = (int)Math.Floor(p.y) };
		}

		public static explicit operator Vector2(SDL.SDL_Point p) {
			return new Vector2(p.x, p.y);
		}

		public override bool Equals(object obj) {
			if(!(obj is Vector2))
				return false;
			Vector2 other = (Vector2)obj;
			return this.x == other.x && this.y == other.y;
		}

		public override int GetHashCode() {
			return base.GetHashCode();
		}

		public override string ToString() {
			return "[" + x + ", " + y + "]";
		}
	}

}

