﻿using System;

namespace Nimbus {
	public struct Color {
		public static readonly Color Grey = new Color(210, 210, 220, 255);
		public static readonly Color White = new Color(255, 255, 255, 255);
		public static readonly Color Black = new Color(0, 0, 0, 255);
		public static readonly Color Red = new Color(255, 0, 0, 255);
		public static readonly Color Green = new Color(0, 255, 0, 255);
		public static readonly Color Blue = new Color(0, 0, 255, 255);

		public byte r, g, b, a;

		public Color(byte r, byte g, byte b, byte a) {
			this.r = r;
			this.g = g;
			this.b = b;
			this.a = a;
		}

		public override string ToString() {
			return "[" + r + ", " + g + ", " + b + ", " + a + "]";
		}

		public static implicit operator SDL2.SDL.SDL_Color(Color c) {
			return new SDL2.SDL.SDL_Color() { r = c.r, g = c.g, b = c.b, a = c.a };
		}
	}
}

