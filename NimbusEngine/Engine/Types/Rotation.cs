﻿using System;

namespace Nimbus {
	public struct Rotation {
		public static readonly Rotation Zero = new Rotation(0f);

		private float angle;
		public float Deg { 
			get { return angle * 180f / (float)Math.PI; }
			set { angle = value * (float)Math.PI / 180f; }
		}

		public float Rad {
			get { return angle; }
			set { angle = value; }
		}

		/// <param name="angle">Degrees of rotation</param>
		private Rotation(float radians) {
			this.angle = radians;
		}

		public static Rotation Degrees(float degrees) {
			return new Rotation(degrees * (float)Math.PI/180f);
		}

		public static Rotation Radians(float radians) {
			return new Rotation(radians);
		}

		public static Rotation operator +(Rotation r1, Rotation r2) {
			return new Rotation(r1.angle + r2.angle);
		}

		public static Rotation operator -(Rotation r1, Rotation r2) {
			return new Rotation(r1.angle - r2.angle);
		}

		public static Rotation operator -(Rotation r) {
			return new Rotation(-r.angle);
		}
		
		public override bool Equals(object obj) {
			if(obj == null || GetType() != obj.GetType()) {
				return false;
			}

			return Rad == ((Rotation)obj).Rad;
		}

		public override int GetHashCode() {
			return base.GetHashCode();
		}

		public static bool operator ==(Rotation r1, Rotation r2) {
			return r1.Equals(r2);
		}

		public static bool operator !=(Rotation r1, Rotation r2) {
			return !r1.Equals(r2);
		}
	}
}

