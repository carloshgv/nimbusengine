﻿using System;

namespace Nimbus {
	public interface ITransformable {
		ITransformable Transform(Transform t);

		Vector2 Center { get; }
	}

	public interface IOverlapable {
		Rectangle BoundingBox { get; }

		Segment[] Edges { get; }

		OverlapInfo Overlaps(IOverlapable other);
	}

	public interface ICollidable : ITransformable, IOverlapable {

	}
}
