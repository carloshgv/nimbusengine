﻿using System;
using System.Linq;

namespace Nimbus {
	public class Polygon : ICollidable {
		public Vector2[] points;
		bool invertedNormals;

		public int NumEdges {
			get {
				return points.Length;
			}
		}

		public Segment[] Edges {
			get {
				Segment[] ret = new Segment[points.Length];
				for(int i = 0; i < points.Length; i++)
					ret[i] = new Segment(points[i], points[(i + 1) % points.Length]);
				return ret;
			}
		}

		public Rectangle BoundingBox {
			get {
				float minx = float.MaxValue,
				miny = float.MaxValue,
				maxx = float.MinValue,
				maxy = float.MinValue;
				for(int i = 0; i < points.Length; i++) {
					if(points[i].x < minx)
						minx = points[i].x;
					if(points[i].x > maxx)
						maxx = points[i].x;
					if(points[i].y < miny)
						miny = points[i].y;
					if(points[i].y > maxy)
						maxy = points[i].y;
				}
				return new Rectangle(minx, miny, maxx - minx, maxy - miny);
			}
		}

		public Vector2 Center {
			get { return points.Aggregate((acc, p) => acc + p) / points.Length; }
		}

		public Polygon(params Vector2[] points) {
			if(points.Length < 3)
				throw new ApplicationException("not enough points for polygon");
			this.points = new Vector2[points.Length];
			points.CopyTo(this.points, 0);
		}

		public Polygon(params float[] coords) {
			if(coords.Length < 6 || coords.Length % 2 != 0)
				throw new ApplicationException("wrong number of coordinates");
			this.points = new Vector2[coords.Length / 2];
			for(int i = 0, j = 0; i < coords.Length - 1; i += 2, j++)
				this.points[j] = new Vector2(coords[i], coords[i + 1]);
		}

		public Polygon(Rectangle rect) {
			points = new Vector2[4];

			points[0].x = rect.x;
			points[0].y = rect.y;

			points[1].x = rect.x + rect.w;
			points[1].y = rect.y;

			points[2].x = rect.x + rect.w;
			points[2].y = rect.y + rect.h;

			points[3].x = rect.x;
			points[3].y = rect.y + rect.h;
		}

		public Segment Edge(int i) {
			if(i > points.Length)
				throw new ArgumentOutOfRangeException();
			return new Segment(points[i], points[(i + 1) % points.Length]);
		}

		public Vector2 Normal(int i) {
			if(i > points.Length)
				throw new ArgumentOutOfRangeException();
			return (points[(i + 1) % points.Length] - points[i]).UnitNormal * ((invertedNormals)?-1:1);
		}

		public static Polygon operator +(Polygon poly, Vector2 v) {
			Polygon res = new Polygon(poly.points);
			for(int i = 0; i < res.points.Length; i++)
				res.points[i] = res.points[i] + v;
			return res;
		}

		public static Polygon operator -(Polygon poly, Vector2 v) {
			Polygon res = new Polygon(poly.points);
			for(int i = 0; i < res.points.Length; i++)
				res.points[i] = res.points[i] - v;
			return res;
		}

		public static Polygon operator *(Polygon poly, Vector2 v) {
			Polygon transformed = new Polygon(Array.ConvertAll(poly.points, point => point * v));
			if (v.x < 0 ^ v.y < 0)
				transformed.invertedNormals = !poly.invertedNormals;
			return transformed;
		}

		public static Polygon operator *(Polygon poly, float f) {
			return new Polygon(Array.ConvertAll(poly.points, point => point * f));
		}

		public static Polygon operator /(Polygon poly, Vector2 v) {
			return new Polygon(Array.ConvertAll(poly.points, point => point / v));
		}

		public static Polygon operator /(Polygon poly, float f) {
			return new Polygon(Array.ConvertAll(poly.points, point => point / f));
		}

		public static Polygon operator *(Polygon poly, Rotation r) {
			return new Polygon(Array.ConvertAll(poly.points, point => point * r));
		}

		public ITransformable Transform(Transform t) {
			return this * t;
		}

		public OverlapInfo Overlaps(IOverlapable other) {
			if(other is Rectangle)
				return MathUtil.SATOverlap(this, other as Rectangle);
			return MathUtil.SATOverlap(this, other as Polygon);
		}

		public override bool Equals(object obj) {
			if(obj == null)
				return false;
			if(!(obj is Polygon))
				return false;
			Polygon other = obj as Polygon;
			if(points.Length != other.points.Length)
				return false;
			for(int i = 0; i < points.Length; i++)
				if(!points[i].Equals(other.points[i]))
					return false;
			return true;
		}

		public override int GetHashCode() {
			return base.GetHashCode();
		}
	}
}

