﻿using System;

namespace Nimbus {
	public struct Segment {
		public Vector2 start, end;

		public Segment(Vector2 start, Vector2 end) {
			this.start = start;
			this.end = end;
		}

		public override string ToString() {
			return "[" + start.x + ", " + start.y + "] -> [" + end.x + ", " + end.y + "]";
		}
	}
}

