﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nimbus.Backend;
using SDL2;
using System.Diagnostics;

namespace Nimbus {
	public partial class Engine {
		int lastId = 0;
		List<Entity> entities;
		List<System> systems;
		List<Entity> markedForDestroy;
		HashSet<Entity> changedEntities;
		List<Nimbus.System> newSystems;
		List<Dictionary<Type, List<Component>>> components;
		Dictionary<System, HashSet<Type>> systemRequeriments;
		Dictionary<System, List<Entity>> systemTargets;

		public Window MainWindow { get { return windows[0]; } }
		public List<Window> windows;

		public AssetManager AssetManager { get; private set; }

		internal event EventHandler StartLoop = delegate { };

		public Engine() {
			entities = new List<Entity>();
			systems = new List<System>();
			markedForDestroy = new List<Entity>();
			changedEntities = new HashSet<Entity>();
			newSystems = new List<Nimbus.System> ();
			components = new List<Dictionary<Type, List<Component>>>();
			systemRequeriments = new Dictionary<System, HashSet<Type>>();
			systemTargets = new Dictionary<System, List<Entity>>();

			Phases.init = new List<System>();
			Phases.framePrefix = new List<System>();
			Phases.preUpdate = new List<System>();
			Phases.preFixedUpdate = new List<System>();
			Phases.fixedUpdate = new List<System>();
			Phases.update = new List<System>();
			Phases.lateUpdate = new List<System>();
			Phases.frameSuffix = new List<System>();
			Phases.render = new List<System>();
			Phases.lateRender = new List<System>();

			levelBuilders = new Dictionary<string, LevelBuilder>();
			entityTemplates = new Dictionary<string, EntityBuilder>();
			windows = new List<Window>();
			StartLoop = (sender, e) => {			
				foreach(var sys in Phases.init)
					sys.Init();
				Time.InitTimer();
			};
			Backend.Libs.Init();
		}

		public Engine(string configuration) : this() {
			Configurator.Apply(configuration, this);
		}

		public void Reset() {
			lastId = 0;
			foreach(var entity in entities)
				DestroyEntity(entity);
			PurgeEntities();
			entities.Clear();

			foreach(var system in systems)
				system.OnDestroy();
			systems.Clear();

			markedForDestroy.Clear();
			changedEntities.Clear();
			components.Clear();
			systemRequeriments.Clear();
			systemTargets.Clear();

			Phases.init.Clear();
			Phases.framePrefix.Clear();
			Phases.preUpdate.Clear();
			Phases.preFixedUpdate.Clear();
			Phases.fixedUpdate.Clear();
			Phases.update.Clear();
			Phases.lateUpdate.Clear();
			Phases.frameSuffix.Clear();
			Phases.render.Clear();
			Phases.lateRender.Clear();
			#if RENDER_DEBUG
			Phases.renderDebug = new List<System>();
			#endif

			collisionSystemCache = null;
		}

		public void MainLoop() {
			for(;;) {
				StartLoop(this, null);
				StartLoop = delegate {
				};
				Time.UpdateTime();
				PurgeEntities();
				UpdateSystemTargets();
				MainWindow.PollEvents();
		
				Time.UseRealDeltaT();
				foreach(var sys in Phases.framePrefix) {
					sys.FramePrefix();
				}

				foreach(var sys in Phases.preUpdate)
					foreach(var ent in systemTargets[sys])
						sys.PreUpdate(ent);

				Time.UseFixedDeltaT();
				do {
					foreach(var sys in Phases.preFixedUpdate)
						foreach(var ent in systemTargets[sys])
							sys.PreFixedUpdate(ent);
					if(collisionSystem != null && Phases.fixedUpdate.Count > 0) {
						collisionSystemCache.FramePrefix();
						foreach(var ent in systemTargets[collisionSystemCache])
							collisionSystemCache.PreUpdate(ent);
					}
					foreach(var sys in Phases.fixedUpdate) {
						foreach(var ent in systemTargets[sys])
							sys.FixedUpdate(ent);
					}
				} while(Time.consumeTime());
				Time.UseRealDeltaT();

				foreach(var sys in Phases.update)
					foreach(var ent in systemTargets[sys])
						sys.Update(ent);

				foreach(var sys in Phases.lateUpdate)
					foreach(var ent in systemTargets[sys])
						sys.LateUpdate(ent);

				foreach(var sys in Phases.frameSuffix) {
					sys.FrameSuffix();
				}

				foreach(Window w in windows)
					w.Clear();
				foreach(var sys in Phases.render)
					foreach(var ent in systemTargets[sys])
						sys.Render(ent);

				foreach(Window w in windows)
					w.FlushDrawQueue();

				foreach(var sys in Phases.lateRender)
					foreach(var ent in systemTargets[sys])
						sys.LateRender(ent);

#if RENDER_DEBUG
				foreach (var sys in Phases.renderDebug)
					foreach (var ent in systemTargets[sys])
						sys.RenderDebug(ent);
#endif

				Console.SetCursorPosition(0, 0);
				Console.WriteLine("{0} fps              ", 1/Time.deltaT);
				Console.WriteLine("{0} deltaT           ", Time.deltaT);

				foreach(Window w in windows)
					w.Present();
			}
		}

		public Window CreateWindow(string title, int width, int height) {
			var newWindow = new Window(title, width, height);
			windows.Add(newWindow);
			AssetManager = new AssetManager(MainWindow);
			return MainWindow;
		}

		public void ConfigureInput(string id, SDL.SDL_Keycode key) {
			Backend.Input.events[id] = false;
			Keyboard.RegisterEvent(id, key);
		}
	}
}

