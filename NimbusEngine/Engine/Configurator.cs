﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;
using SDL2;
using System.Globalization;
using TiledSharp;
using Nimbus.Assets;

namespace Nimbus {
	public static class Configurator {
		public static Dictionary<string, object> createdObjects;

		struct ConstructionInfo {
			internal Type type;
			object[] param;
			bool hasProxies;

			internal object[] parameters {
				get {
					if(hasProxies) {
						object[] resolved = new object[param.Length];
						for(int i = 0; i < param.Length; i++)
							if(param[i] != null && param[i].GetType() == typeof(Engine.EntityProxy))
								resolved[i] = ((Engine.EntityProxy)param[i]).Resolve();
							else
								resolved[i] = param[i];
						return resolved;
					} else
						return param;
				}
				set {
					param = value;
					hasProxies = false;
					for(int i = 0; i < param.Length && !hasProxies; i++)
						if(param[i] != null && param[i].GetType() == typeof(Engine.EntityProxy))
							hasProxies = true;
				}
			}
		}

		public static void Apply(string filename, Engine target) {
			XElement game = XElement.Load(filename);
			createdObjects = new Dictionary<string, object>();

			var globalConfig = game.Element("configuration");
			var title = game.Attribute("name").Value;
			// Create windows
			var windows = 
				from window in globalConfig.Elements("window")
				select new { id = (string)window.Attribute("id"),
				             width = (int)window.Attribute("width"),
				             height = (int)window.Attribute("height"),
				             ppu = (int?)window.Attribute("pixels-per-unit"),
				             scale = (float?)window.Attribute("scale"),
				             fullscreen = (bool?)window.Attribute("fullscreen") };
			foreach(var window in windows) {
				Backend.Window newWindow = target.CreateWindow(title, window.width, window.height);
				if(window.ppu != null) {
					newWindow.pixelsPerUnit = (int)window.ppu;
					newWindow.unitsPerPixel = 1f / (float)window.ppu;
				}
				if(window.fullscreen != null)
					newWindow.Fullscreen((bool)window.fullscreen);
				if(window.scale != null)
					newWindow.SetScale((float)window.scale);
				createdObjects[window.id] = newWindow;
			}

			// Configure inputs
			var inputs = 
				from input in globalConfig.Elements("input")
				select new { id = (string)input.Attribute("id"),
				             key = "SDLK_" + (string)input.Attribute("key") };
			foreach(var input in inputs) {
				var keyCode = (SDL.SDL_Keycode)typeof(SDL.SDL_Keycode).GetField(input.key).GetValue(null);
				target.ConfigureInput(input.id, keyCode);
			}

			// Load Assets
			var assets =
				from asset in game.Element("assets").Elements()
				select new { type = (string)asset.Name.LocalName,
				             id = (string)asset.Attribute("id"),
				             source = (string)asset.Attribute("source"),
							 raw = asset };
			foreach(var asset in assets) {
				switch(asset.type) {
				case "texture":
					createdObjects[asset.id] = target.AssetManager.LoadTexture(asset.source);
					break;
				case "spritesheet":
					createdObjects[asset.id] = target.AssetManager.LoadSheetDescriptor(asset.source);
					break;
				case "audio":
					createdObjects[asset.id] = target.AssetManager.LoadAudio(asset.source);
					break;
				case "truetypefont":
					createdObjects[asset.id] = target.AssetManager.LoadTrueTypeFont(asset.source, (int)asset.raw.Attribute("size"));
					break;
				}
			}

			// Register templates
			foreach(var template in game.Element("templates").Elements()) {
				string id = (string)template.Attribute("id");
				string tag = (string)template.Attribute("tag");
				LayerMask mask = LayerMask.All;
				string layers = (string)template.Attribute("layers");
				if(layers != null) {
					int[] layerNumbers = layers.Split(',').Select(n => int.Parse(n)).ToArray();
					mask = LayerMask.Create(layerNumbers);
				}
				List<ConstructionInfo> componentsInfo = new List<ConstructionInfo>();
				foreach(var component in template.Elements("component"))
					componentsInfo.Add(GetConstructionInfo(component, target));
				target.RegisterTemplate(id, () => {
					return target.CreateEntity(tag ?? id, mask,
					                           componentsInfo.Select(c => Activator.CreateInstance(c.type, c.parameters) as Nimbus.Component).ToArray());
				});
			}

			// Load levels
			foreach(var level in game.Elements("level"))
				ConfigureLevel(target, level, game.Element("templates").Elements());
		}

		static void ConfigureLevel(Engine target, XElement levelConfig, IEnumerable<XElement> templates) {
			var id = (string)levelConfig.Attribute("id");
			List<ConstructionInfo> systemsInfo = new List<ConstructionInfo>();

			// Read systems
			foreach(var system in levelConfig.Elements("system")) {
				systemsInfo.Add(GetConstructionInfo(system, target));
			}

			// Get Camera
			var cameraElement = levelConfig.Element("camera");
			string camera = null;
			if(cameraElement != null)
				camera = cameraElement.Value.Substring(1);

			// Parse TMX
			XElement mapAssetName = levelConfig.Element("map");
			Texture[] tiles = null;
			Texture[] backgrounds = null;
			int[] backgroundsZorder = null;
			Vector2[] backgroundsParallax = null;
			TmxMap tmx = null;
			if(mapAssetName != null) {
				tmx = target.AssetManager.LoadTiledMap(mapAssetName.Value);
				tiles = new Texture[tmx.Tilesets.Count];
				for(int i = 0; i < tmx.Tilesets.Count; i++)
					tiles[i] = target.AssetManager.LoadTexture(tmx.Tilesets[i].Image.Source);
				if(tmx.ImageLayers.Count > 0) {
					backgrounds = new Texture[tmx.ImageLayers.Count];
					backgroundsZorder = new int[tmx.ImageLayers.Count];
					backgroundsParallax = new Vector2[tmx.ImageLayers.Count];
					for(int i = 0; i < tmx.ImageLayers.Count; i++) {
						backgrounds[i] = target.AssetManager.LoadTexture(tmx.ImageLayers[i].Image.Source);
						string zorder;
						if(!tmx.ImageLayers[i].Properties.TryGetValue("zorder", out zorder) || !int.TryParse(zorder, out backgroundsZorder[i]))
						   backgroundsZorder[i] = int.MaxValue - i;
						string parallaxX, parallaxY;
						if(!tmx.ImageLayers[i].Properties.TryGetValue("parallaxX", out parallaxX) || !float.TryParse(parallaxX, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out backgroundsParallax[i].x))
							backgroundsParallax[i].x = 1;
						if(!tmx.ImageLayers[i].Properties.TryGetValue("parallaxY", out parallaxY) || !float.TryParse(parallaxY, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out backgroundsParallax[i].y))
							backgroundsParallax[i].y = 1;
					}
				}
			}

			// Store level builder
			target.levelBuilders[id] = () => {
				foreach(var info in systemsInfo)
					target.AddSystem(Activator.CreateInstance(info.type, info.parameters) as Nimbus.System);

				if(tmx != null) {
					// Map background
					target.CreateEntity(new TiledMap(tiles, tmx));

					// Colliders
					var collisionGroup = tmx.ObjectGroups.Where(og => og.Name == "collision").SingleOrDefault();
					if(collisionGroup != null) {
						foreach(var obj in collisionGroup.Objects) {
							Entity collisionEntity = null;
							var template = templates.Where(t => (string)t.Attribute("id") == obj.Type).SingleOrDefault();
							if(template != null) {
								collisionEntity = target.CreateEntityFromTemplate(obj.Type);
								string layers, tag;
								if(obj.Properties.TryGetValue("tag", out tag))
									collisionEntity.tag = String.Copy(tag);
								if(obj.Properties.TryGetValue("layers", out layers)) {
									int[] layerNumbers = layers.Split(',').Select(n => int.Parse(n)).ToArray();
									collisionEntity.layerMask = LayerMask.Create(layerNumbers);
								}
							} else {
								collisionEntity = target.CreateEntity(obj.Type);
							}
							collisionEntity.AddComponent(new Transform(new Vector2(obj.X, obj.Y) / target.MainWindow.pixelsPerUnit));
							switch(obj.ObjectType) {
							case TmxObjectGroup.TmxObjectType.Basic:
								collisionEntity.AddComponent(new Collider(new Rectangle(0, 0, obj.Width, obj.Height) / target.MainWindow.pixelsPerUnit));
								break;
							case TmxObjectGroup.TmxObjectType.Polygon:
								collisionEntity.AddComponent(new Collider(new Polygon(obj.Points.Select(p => new Vector2(p.Item1, p.Item2)).ToArray()) / target.MainWindow.pixelsPerUnit));
								break;
							}
						}
					}

					// Entities
					var entitiesGroup = tmx.ObjectGroups.Where(og => og.Name == "entities").SingleOrDefault();
					if(entitiesGroup != null) {
						foreach(var obj in entitiesGroup.Objects) {
							Entity entity = target.CreateEntityFromTemplate(obj.Type);
							if(entity.transform == null)
								entity.AddComponent(new Transform());
							entity.transform.position = new Vector2(obj.X, obj.Y) / target.MainWindow.pixelsPerUnit;
							string layers, tag;
							if(obj.Properties.TryGetValue("tag", out tag))
								entity.tag = String.Copy(tag);
							if(obj.Properties.TryGetValue("layers", out layers)) {
								int[] layerNumbers = layers.Split(',').Select(n => int.Parse(n)).ToArray();
								entity.layerMask = LayerMask.Create(layerNumbers);
							}
						}
					}
				}

				// Set camera
				if(camera != null) {
					Entity cameraEntity = target.FindEntityByTag(camera);
					if(cameraEntity == null)
						cameraEntity = target.CreateEntityFromTemplate(camera);
					if(backgrounds != null) {
						for(int i = 0; i < backgrounds.Length; i++)
							cameraEntity.GetComponent<Camera>().AddBackgroundLayer(backgrounds[i],
							                                                       backgroundsParallax[i],
							                                                       backgroundsZorder[i]);
					}
					target.MainWindow.SetCamera(cameraEntity);
				}
			};
		}

		static object ParseTo(string value, Type type) {
			if(type == typeof(float))
				return float.Parse(value, CultureInfo.InvariantCulture);
			else if(type == typeof(int))
				return int.Parse(value);
			else if(type == typeof(bool))
				return bool.Parse(value);
			else if(type == typeof(string))
				return value;
			else if(type == typeof(ICollidable)) {
				string[] declaration = value.Split(':');
				string[] values = declaration[1].Split(',');
				switch(declaration[0]) {
				case "rectangle":
					if(values.Length < 4)
						throw new ApplicationException("not enough values for rectangle (" + value + ")");
					return new Rectangle(
						float.Parse(values[0], CultureInfo.InvariantCulture),
						float.Parse(values[1], CultureInfo.InvariantCulture),
						float.Parse(values[2], CultureInfo.InvariantCulture),
						float.Parse(values[3], CultureInfo.InvariantCulture));
				case "polygon":
					return new Polygon(values.Select(v => float.Parse(v, CultureInfo.InvariantCulture)).ToArray());
				default:
					throw new ApplicationException("wrong type when parsing " + value + ", expected 'polygon' or 'rectangle'");
				}
			} else if(type == typeof(Vector2?) || type == typeof(Vector2)) {
				string[] declaration = value.Split(':');
				if(declaration[0] != "vector2")
					throw new ApplicationException("wrong type when parsing " + value + ", expected 'vector2'");
				string[] values = declaration[1].Split(',');
				return new Vector2(
					float.Parse(values[0]),
					float.Parse(values[1]));
			} else if(type == typeof(Color) || type == typeof(Color?)) {
				string[] declaration = value.Split(':');
				if(declaration[0] != "color")
					throw new ApplicationException("wrong type when parsing " + value + ", expected 'color'");
				string[] values = declaration[1].Split(',');
				return new Color(byte.Parse(values[0]), byte.Parse(values[1]), byte.Parse(values[2]), byte.Parse(values[3]));
			} else if(type == typeof(AnimState[])) {
				string[] declarations = value.Split(';').Select(s => s.Trim()).ToArray();
				AnimState[] states = new AnimState[declarations.Length];
				for(int i = 0; i < states.Length; i++) {
					var declaration = declarations[i].Split(':');
					if(declaration[0] != "animstate")
						throw new ApplicationException("wrong type when parsing " + value + ", expected 'animstate'");
					states[i].id = declaration[1];
					states[i].frames = declaration[2].Split(',').Select(n => int.Parse(n)).ToArray();
				}
				return states;
			} else if(type == typeof(Type)) {
				Type t = Type.GetType(value);
				if(t == null)
					throw new ApplicationException("couldn't find type " + value);
				return t;
			} else if(type == typeof(AudioSource.DecayFunction)) {
				var function = typeof(AudioSource.DecayCurves).GetMethod(value, BindingFlags.Static | BindingFlags.Public);
				if(function == null)
					throw new ApplicationException("couldn't find decay function " + value);
				var del = function.CreateDelegate(typeof(AudioSource.DecayFunction)) as AudioSource.DecayFunction;
				if(del == null)
					throw new ApplicationException("couldn't create delegate " + value);
				return del;
			}

			throw new ApplicationException("couldn't parse " + value + " as " + type);
		}

		static ConstructionInfo GetConstructionInfo(XElement description, Engine target) {
			Type type = Type.GetType((string)description.Attribute("class"));
			if(type == null)
				throw new ApplicationException("system " + (string)description.Attribute("class") + " not found");
			var constructors = type.GetConstructors().Where(c => c.GetParameters().Count() >= description.Elements("param").Count());
			foreach(var constructor in constructors) {
				try {
					ParameterInfo[] constructorParameters = constructor.GetParameters();
					object[] parameterValues = new object[constructorParameters.Length];
					for(int i = 0; i < constructorParameters.Length; i++) {
						string value =
							(from param in description.Elements("param")
							 where (string)param.Attribute("name") == constructorParameters[i].Name
							 select param.Value.Trim()).SingleOrDefault();
						if(value == null) {
							if(constructorParameters[i].HasDefaultValue)
								parameterValues[i] = constructorParameters[i].DefaultValue;
							else
								throw new ArgumentException("needed argument not found");
						} else if(value[0] == '*') {
							if(constructorParameters[i].ParameterType == typeof(Entity))
								parameterValues[i] = new Engine.EntityProxy(value.Substring(1), target);
							else
								parameterValues[i] = createdObjects[value.Substring(1)];
						} else
							parameterValues[i] = ParseTo(value, constructorParameters[i].ParameterType);
					}
					return new ConstructionInfo() { type = type, parameters = parameterValues };
				} catch(ArgumentException) {
					continue;
				}
			}
			throw new ApplicationException("couldn't create object " + type);
		}
	}
}

