using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using SDL2;
using Nimbus;
using Nimbus.Assets;

namespace Nimbus {
	namespace Backend {
		public class Window {
			public event EventHandler CloseEvent = delegate { };

			public Rectangle Viewport {
				get {
					return screen;
				}
			}

			public float PixelsPerUnit { get { return pixelsPerUnit; } }

			public float UnitsPerPixel { get { return unitsPerPixel; } }

			internal float pixelsPerUnit = 100f;
			internal float unitsPerPixel = 0.01f;

			struct DrawParams {
				internal Texture texture;
				internal Rectangle src; 
				internal Rectangle dst; 
				internal float angle;
				internal Vector2 center; 
				internal SDL.SDL_RendererFlip flip;
			}

			IntPtr window;
			IntPtr renderer;
			Transform cameraTransform;
			Camera cameraComponent;
			Rectangle screen;
			float scale;
			List<Tuple<int, DrawParams>> drawQueue;
			bool fullscreen;

			public Window(string title, int width, int height) {
				window = SDL.SDL_CreateWindow(title, SDL.SDL_WINDOWPOS_CENTERED, SDL.SDL_WINDOWPOS_CENTERED, width, height, SDL.SDL_WindowFlags.SDL_WINDOW_SHOWN);
				screen = new Rectangle(0, 0, width, height);
				if(window == IntPtr.Zero)
					throw new ApplicationException("error creating window" + SDL.SDL_GetError());
				renderer = SDL.SDL_CreateRenderer(window, -1, (uint)SDL.SDL_RendererFlags.SDL_RENDERER_ACCELERATED |
				                                              (uint)SDL.SDL_RendererFlags.SDL_RENDERER_PRESENTVSYNC);
				if(renderer == IntPtr.Zero)
					throw new ApplicationException("error creating renderer" + SDL.SDL_GetError());
				SDL.SDL_RenderSetLogicalSize(renderer, width, height);
				SDL.SDL_SetRenderDrawBlendMode(renderer, SDL.SDL_BlendMode.SDL_BLENDMODE_BLEND);
				CloseEvent += (sender, e) => SDL_mixer.Mix_CloseAudio();
				drawQueue = new List<Tuple<int, DrawParams>>();
				fullscreen = false;

				cameraTransform = new Transform();
				cameraComponent = new Camera();
			}

			public void ToggleFullscreen() {
				fullscreen = !fullscreen;
				Fullscreen(fullscreen);
			}

			public void Fullscreen(bool newMode) {
				fullscreen = newMode;
				if(fullscreen) {
					SDL.SDL_SetWindowFullscreen(window, (uint)SDL.SDL_WindowFlags.SDL_WINDOW_FULLSCREEN_DESKTOP);
				} else {
					SDL.SDL_SetWindowFullscreen(window, 0);
				}
			}

			public void SetScale(float newScale) {
				if(!fullscreen) {
					scale = newScale;
					SDL.SDL_SetWindowSize(window, (int)(screen.w * scale), (int)(screen.h * scale));
					SDL.SDL_SetWindowPosition(window, SDL.SDL_WINDOWPOS_CENTERED, SDL.SDL_WINDOWPOS_CENTERED);
				}
			}

			public void SetTitle(string newTitle) {
				SDL.SDL_SetWindowTitle(window, newTitle);
			}

			public void SetCamera(Entity e) {
				cameraTransform = e.transform;
				cameraComponent = e.GetComponent<Camera>();
			}

			public void Clear() {
				SDL.SDL_SetRenderDrawColor(renderer, 
				                           cameraComponent.backgroundColor.r, cameraComponent.backgroundColor.g, cameraComponent.backgroundColor.b, 
				                           cameraComponent.backgroundColor.a);
				SDL.SDL_RenderClear(renderer);
			}

			public void SetColor(byte r, byte g, byte b, byte a) {
				SDL.SDL_SetRenderDrawColor(renderer, r, g, b, a);
			}

			public void Fill(Rectangle worldRect) {
				Rectangle screenRect = (worldRect - cameraTransform.position) * pixelsPerUnit;
				if(Viewport.Overlaps(screenRect)) { 
					SDL.SDL_Rect r = screenRect;
					SDL.SDL_RenderFillRect(renderer, ref r);
				}
			}

			public void Draw(Rectangle worldRect) {
				Rectangle screenRect = (worldRect - cameraTransform.position) * pixelsPerUnit;
				if(Viewport.Overlaps(screenRect)) {
					SDL.SDL_Rect r = screenRect;
					SDL.SDL_RenderDrawRect(renderer, ref r);
				}
			}

			public void Fill(Polygon poly) {
				Fill(poly.BoundingBox);
				Draw(poly);
			}

			public void Draw(Polygon worldPoly) {
				Polygon screenPoly = (worldPoly - cameraTransform.position) * pixelsPerUnit;
				if(Viewport.Overlaps(screenPoly.BoundingBox)) {
					SDL.SDL_RenderDrawLines(renderer, Array.ConvertAll(screenPoly.points, i => (SDL.SDL_Point)i), screenPoly.NumEdges);
					SDL.SDL_RenderDrawLine(renderer, (int)screenPoly.points[screenPoly.NumEdges - 1].x, (int)screenPoly.points[screenPoly.NumEdges - 1].y, 
					                       (int)screenPoly.points[0].x, (int)screenPoly.points[0].y);
				}
			}

			public void Draw(Texture texture, Rectangle src, Rectangle dst, float angle, Vector2 center, SDL.SDL_RendererFlip flip) {
				Rectangle viewDst = dst - cameraTransform.position * pixelsPerUnit;
				if(Viewport.Overlaps(viewDst)) {
					SDL.SDL_Rect srcRect = src;
					SDL.SDL_Point point = center;
					SDL.SDL_Rect corrected = viewDst;
					SDL.SDL_RenderCopyEx(renderer, texture.data, ref srcRect, ref corrected, angle, ref point, flip);
				}
			}

			public void Draw(Texture texture, Rectangle dst, float angle, Vector2 center, SDL.SDL_RendererFlip flip) {
				Draw(texture, texture.rect, dst, angle, center, flip);
			}

			public void QueueDraw(int priority, Texture texture, Rectangle src, Rectangle dst, float angle, Vector2 center, SDL.SDL_RendererFlip flip) {
				drawQueue.Add(Tuple.Create(priority, new DrawParams() { texture = texture, src = new Rectangle(src), dst = new Rectangle(dst), angle = angle, center = center, flip = flip }));
			}

			public void QueueDraw(int priority, Texture texture, Rectangle dst, float angle, Vector2 center, SDL.SDL_RendererFlip flip) {
				drawQueue.Add(Tuple.Create(priority, new DrawParams() { texture = texture, src = texture.rect, dst = new Rectangle(dst), angle = angle, center = center, flip = flip }));
			}

			public void WriteText(TrueTypeFont font, string text, Vector2 point, Color? color) {
				Color fg = color ?? Color.White;
				IntPtr surfacePtr = SDL_ttf.TTF_RenderUTF8_Blended(font.data, text, fg);
				SDL.SDL_Surface surface = (SDL.SDL_Surface)Marshal.PtrToStructure(surfacePtr, typeof(SDL.SDL_Surface));
				SDL.SDL_Rect src = new SDL.SDL_Rect() { x = 0, y = 0, w = surface.w, h = surface.h };
				SDL.SDL_Rect dst = new Rectangle(point.x, point.y, surface.w, surface.h) - cameraTransform.position * pixelsPerUnit;
				IntPtr texture = SDL.SDL_CreateTextureFromSurface(renderer, surfacePtr);
				SDL.SDL_RenderCopy(renderer, texture, ref src, ref dst);
				SDL.SDL_DestroyTexture(texture);
				SDL.SDL_FreeSurface(surfacePtr);
			}

			public Texture LoadTexture(string filename) {
				IntPtr surface = SDL_image.IMG_Load(filename);
				if(surface == IntPtr.Zero)
					throw new ApplicationException("failed loading file " + filename + ": " + SDL.SDL_GetError());
				IntPtr data = SDL.SDL_CreateTextureFromSurface(renderer, surface);
				if(data == IntPtr.Zero)
					throw new ApplicationException("failed creating texture " + filename + ": " + SDL.SDL_GetError());
				SDL.SDL_FreeSurface(surface);
				SDL.SDL_Rect rect = new SDL.SDL_Rect();
				uint format;
				int access;
				SDL.SDL_QueryTexture(data, out format, out access, out rect.w, out rect.h);
				rect.x = 0;
				rect.y = 0;
				return new Texture(data, (Rectangle)rect);
			}

			public void Present() {
				SDL.SDL_RenderPresent(renderer);
			}

			public void FlushDrawQueue() {
				drawQueue.Sort((a, b) => { if(a.Item1 < b.Item1) return 1; else if(a.Item1 > b.Item1) return -1; else return 0; });
				foreach(var call in drawQueue) {
					DrawParams parameters = call.Item2;
					Draw(parameters.texture, parameters.src, parameters.dst, parameters.angle, parameters.center, parameters.flip);
				}
				drawQueue.Clear();
			}

			public void PollEvents() {
				SDL.SDL_Event ev;

				SDL.SDL_PollEvent(out ev);
				Input.Clean();
				Keyboard.Poll(ev);
				if(ev.type == SDL.SDL_EventType.SDL_QUIT)
					CloseEvent(this, EventArgs.Empty);
			}
		}

		public static class Keyboard {
			static Dictionary<SDL.SDL_Keycode, string> assignments;

			static Keyboard() {
				assignments = new Dictionary<SDL.SDL_Keycode, string>();
			}

			public static void RegisterEvent(string id, SDL.SDL_Keycode key) {
				assignments[key] = id;
			}

			internal static void Poll(SDL.SDL_Event ev) {
				if(ev.type == SDL.SDL_EventType.SDL_KEYDOWN && assignments.ContainsKey(ev.key.keysym.sym)) {
					Input.press[assignments[ev.key.keysym.sym]] = true;
					Input.events[assignments[ev.key.keysym.sym]] = true;
				} else if(ev.type == SDL.SDL_EventType.SDL_KEYUP && assignments.ContainsKey(ev.key.keysym.sym)) {
					Input.release[assignments[ev.key.keysym.sym]] = true;
					Input.events[assignments[ev.key.keysym.sym]] = false;
				}
			}
		}

		public static class Input {
			static internal Dictionary<string, bool> events;
			static internal Dictionary<string, bool> press;
			static internal Dictionary<string, bool> release;

			static Input() {
				events = new Dictionary<string, bool>();
				press = new Dictionary<string, bool>();
				release = new Dictionary<string, bool>();
			}

			internal static void Clean() {
				press.Clear();
				release.Clear();
			}

			public static bool Get(string id) {
				bool value;
				events.TryGetValue(id, out value);
				return value;
			}

			public static bool Pressed(string id) {
				bool value;
				press.TryGetValue(id, out value);
				return value;
			}

			public static bool Released(string id) {
				bool value;
				release.TryGetValue(id, out value);
				return value;
			}
		}

		public static class Sound {
			internal static SDL_mixer.ChannelFinishedDelegate haltCallback = HaltCallback;
			internal delegate void ChannelHalt(int channel);
			internal static event ChannelHalt OnChannelHalt;

			public static readonly int NUM_CHANNELS = 32;

			public static int Play(IntPtr sound, int channel = -1, bool loop = false) {
				return SDL_mixer.Mix_PlayChannel(-1, sound, loop ? -1 : 0);
			}

			public static void Stop(int channel) {
				SDL_mixer.Mix_HaltChannel(channel);
			}

			public static void StopAll() {
				SDL_mixer.Mix_HaltChannel(-1);
			}

			public static void Pause(int channel) {
				SDL_mixer.Mix_Pause(channel);
			}

			public static void SetVolume(int channel, float volume) {
				if(volume < 0)
					volume = 0;
				else if(volume > 1)
					volume = 1;
				SDL_mixer.Mix_Volume(channel, (int)(volume * SDL_mixer.MIX_MAX_VOLUME));
			}
				
			// position: -1 left 0 center 1 right
			public static void SetPanning(int channel, float position) {
				if(position > 1)
					position = 1;
				if(position < -1)
					position = -1;
				byte left, right;
				if(position < 0) {
					left = 255;
					right = (byte)(255 + 255 * position);
				} else if(position > 0) {
					left = (byte)(255 - 255 * position);
					right = 255;
				} else
					left = right = 255;

				SDL_mixer.Mix_SetPanning(channel, left, right);
			}

			public static void HaltCallback(int channel) {
				OnChannelHalt(channel);
			}
		}

		public static class Libs {
			public static int Init() {
				int retcode;

				if((retcode = SDL.SDL_Init(SDL.SDL_INIT_AUDIO | SDL.SDL_INIT_VIDEO)) < 0)
					throw new ApplicationException("error initializing SDL2: " + SDL.SDL_GetError());
				if((retcode = SDL_image.IMG_Init(SDL_image.IMG_InitFlags.IMG_INIT_JPG |
				                                 SDL_image.IMG_InitFlags.IMG_INIT_PNG |
				                                 SDL_image.IMG_InitFlags.IMG_INIT_TIF |
				                                 SDL_image.IMG_InitFlags.IMG_INIT_WEBP)) < 0)
					throw new ApplicationException("error initializing SDL_image: " + SDL.SDL_GetError());
				if((retcode = SDL_mixer.Mix_Init(SDL_mixer.MIX_InitFlags.MIX_INIT_MP3 |
				                                 SDL_mixer.MIX_InitFlags.MIX_INIT_OGG |
				                                 SDL_mixer.MIX_InitFlags.MIX_INIT_FLAC |
				                                 SDL_mixer.MIX_InitFlags.MIX_INIT_MOD)) < 0)
					throw new ApplicationException("error initializing SDL_mixer: " + SDL.SDL_GetError());
				if ((retcode = SDL_ttf.TTF_Init ()) < 0)
					throw new ApplicationException ("error initializing SDL_ttf: " + SDL.SDL_GetError ());
				if((retcode = SDL_mixer.Mix_OpenAudio(44100, SDL_mixer.MIX_DEFAULT_FORMAT, 2, 4096)) < 0)
					throw new ApplicationException("error opening audio device: " + SDL.SDL_GetError());
				if((retcode = SDL_mixer.Mix_AllocateChannels(Sound.NUM_CHANNELS)) < 0)
					throw new ApplicationException("error allocating channels: " + SDL.SDL_GetError());
				SDL_mixer.Mix_ChannelFinished(Sound.haltCallback);
				return retcode;
			}
		}
	}
}