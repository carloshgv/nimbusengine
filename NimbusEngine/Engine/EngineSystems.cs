﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nimbus.Backend;
using SDL2;
using System.Diagnostics;

namespace Nimbus {
	public partial class Engine {
		internal Collision collisionSystemCache;

		internal Collision collisionSystem {
			get {
				if(collisionSystemCache == null)
					collisionSystemCache = GetSystem<Collision>();
				return collisionSystemCache;
			}
		}

		struct Phases {
			static internal List<System> init;
			static internal List<System> framePrefix;
			static internal List<System> preUpdate;
			static internal List<System> preFixedUpdate;
			static internal List<System> fixedUpdate;
			static internal List<System> update;
			static internal List<System> lateUpdate;
			static internal List<System> frameSuffix;
			static internal List<System> render;
			static internal List<System> lateRender;
			#if RENDER_DEBUG
			static internal List<System> renderDebug;
			#endif
		}

		public System AddSystem(System s) {
			RequiresAttribute requires = s.GetType().GetCustomAttributes(true).FirstOrDefault() as RequiresAttribute;
			systemRequeriments[s] = (requires != null) ? requires.requires : new HashSet<Type>();
			systemTargets[s] = new List<Entity>();
			systems.Add(s);
			s.owner = this;
			s.targets = systemTargets[s];

			newSystems.Add (s);

			if(s.GetType().GetMethod("Init").DeclaringType == s.GetType())
				Phases.init.Add(s);
			if(s.GetType().GetMethod("FramePrefix").DeclaringType == s.GetType())
				Phases.framePrefix.Add(s);
			if(s.GetType().GetMethod("PreUpdate").DeclaringType == s.GetType())
				Phases.preUpdate.Add(s);
			if(s.GetType().GetMethod("PreFixedUpdate").DeclaringType == s.GetType())
				Phases.preFixedUpdate.Add(s);
			if(s.GetType().GetMethod("FixedUpdate").DeclaringType == s.GetType())
				Phases.fixedUpdate.Add(s);
			if(s.GetType().GetMethod("Update").DeclaringType == s.GetType())
				Phases.update.Add(s);
			if(s.GetType().GetMethod("LateUpdate").DeclaringType == s.GetType())
				Phases.lateUpdate.Add(s);
			if(s.GetType().GetMethod("FrameSuffix").DeclaringType == s.GetType())
				Phases.frameSuffix.Add(s);
			if(s.GetType().GetMethod("Render").DeclaringType == s.GetType())
				Phases.render.Add(s);
			if(s.GetType().GetMethod("LateRender").DeclaringType == s.GetType())
				Phases.lateRender.Add(s);
			#if RENDER_DEBUG
			if (s.GetType().GetMethod("RenderDebug").DeclaringType == s.GetType())
				Phases.renderDebug.Add(s);
			#endif
			return s;
		}

		public T GetSystem<T>() {
			return (T)Convert.ChangeType(systems.FirstOrDefault(s => s.GetType() == typeof(T)), typeof(T));
		}

		public void DestroySystem(System s) {
			s.OnDestroy();
			systems.Remove(s);
			systemRequeriments.Remove(s);
			systemTargets.Remove(s);
			collisionSystemCache = null;

			Phases.init.Remove(s);
			Phases.framePrefix.Remove(s);
			Phases.preUpdate.Remove(s);
			Phases.preFixedUpdate.Remove(s);
			Phases.fixedUpdate.Remove(s);
			Phases.update.Remove(s);
			Phases.lateUpdate.Remove(s);
			Phases.frameSuffix.Remove(s);
			Phases.render.Remove(s);
			Phases.lateRender.Remove(s);
			#if RENDER_DEBUG
			Phases.renderDebug.Remove(s);
			#endif
		}

		void UpdateSystemTargets(Nimbus.System system) {
			foreach(var e in entities) {
				systemTargets[system].Remove(e);
				if(systemRequeriments[system].IsSubsetOf(components[e.id].Keys))
					systemTargets[system].Add(e);
			}
		}

		void UpdateSystemTargets(Entity e) {
			foreach(var sys in systemTargets) {
				sys.Value.Remove(e);
				if(systemRequeriments[sys.Key].IsSubsetOf(components[e.id].Keys))
					sys.Value.Add(e);
			}
		}
	
		void UpdateSystemTargets() {
			foreach (var sys in newSystems)
				UpdateSystemTargets (sys);
			foreach (var entity in changedEntities)
				UpdateSystemTargets (entity);
			newSystems.Clear ();
			changedEntities.Clear ();
		}
	}
}

