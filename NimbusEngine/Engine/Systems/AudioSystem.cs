﻿using System;

namespace Nimbus {
	[Requires(typeof(AudioSource))]
	public class AudioSystem : Nimbus.System {
		Transform listenerTransform;
		AudioListener listener;
		AudioSource[] channels;

		public AudioSystem() {
			channels = new AudioSource[Backend.Sound.NUM_CHANNELS];
		}

		public override void Init() {
			Backend.Sound.OnChannelHalt += ChannelHalt;
			var listeners = GetEntitiesWithComponent<AudioListener>();
			if(listeners == null)
				throw new ApplicationException("AudioSystem needs an AudioListener entity");
			listener = GetEntitiesWithComponent<AudioListener>()[0].GetComponent<AudioListener>();
			listenerTransform = GetEntitiesWithComponent<AudioListener>()[0].GetComponent<Transform>();
			foreach(var e in targets)
				foreach(var source in e.GetComponents<AudioSource>()) {
					source.OnPlayRequest += PlaySource;
					if(source.playOnStart) {
						source.Play();
						if(source.channel >= 0)
							AdjustChannelParameters(source);
					}
				}
		}

		public override void FrameSuffix() {
			for(int i = 0; i < channels.Length; i++)
				if(channels[i] != null)
					AdjustChannelParameters(channels[i]);
		}

		void AdjustChannelParameters(AudioSource source) {
			float xDistance = (source.entity.transform.position.x - listenerTransform.position.x) / MainWindow.Viewport.Size.x;
			float absDistance = Math.Abs(source.entity.transform.position.Distance(listenerTransform.position));
			float volume = source.Decay(absDistance, source.radius) * source.volume * listener.amplification;
			if(volume < listener.threshold)
				volume = 0;

			Backend.Sound.SetPanning(source.channel, xDistance);
			Backend.Sound.SetVolume(source.channel, volume);
		}

		void PlaySource(AudioSource source) {
			int channel = Backend.Sound.Play(source.audio.data, -1, source.loop);
			if(channel >= 0) {
				source.channel = channel;
				channels[channel] = source;
				AdjustChannelParameters(source);
			}
		}

		void ChannelHalt(int channel) {
			channels[channel].channel = -1;
			channels[channel] = null;
		}

		public override void OnDestroy() {
			Backend.Sound.StopAll();
			Backend.Sound.OnChannelHalt -= ChannelHalt;
		}
	}
}

