﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Nimbus {
	[Requires(typeof(Transform), typeof(Collider), typeof(RigidBody))]
	public class Physics : System {
		public float gravity;
		public float worldResistance;

		public Physics(float gravity = 10f, float worldResistance = 0.5f) {
			this.gravity = gravity;
			this.worldResistance = worldResistance;
		}

		public override void PreFixedUpdate(Entity entity) {
			RigidBody body = entity.rigidbody;
			if(body.Disabled)
				return;
			Transform transform = entity.transform;
			Collider collider = entity.collider;

			if(!body.IsPlatform) {
				// move object
				transform.position += body.velocity * Time.deltaT;

				// update velocity
				Vector2 prev = body.velocity;
				body.velocity += (Vector2.Left * body.velocity * body.drag * worldResistance + Vector2.Down * gravity * body.gravity) * Time.deltaT;
				if(Math.Abs(prev.x) > Math.Abs(body.velocity.x) && Math.Abs(body.velocity.x) < 0.0005f)
				   body.velocity.x = 0;
				if(Math.Abs(prev.y) > Math.Abs(body.velocity.y) && Math.Abs(body.velocity.y) < 0.0005f)
				   body.velocity.y = 0;
			}
		}

		public override void FixedUpdate(Entity entity) {
			RigidBody body = entity.rigidbody;
			if(body.Disabled)
				return;
			Transform transform = entity.transform;
			Collider collider = entity.collider;

			List<CollisionInfo> collisions = collider.Collisions.FindAll(o => (entity.layerMask & o.entity.layerMask) && o.entity.rigidbody != null && !o.entity.rigidbody.Disabled && o.collider == o.entity.colliders[0]);
			foreach(var info in collisions.Where(info => entity.id < info.entity.id)) {
				if(body.IsPlatform && info.entity.rigidbody.IsPlatform)
					continue;
				else if(!body.IsPlatform && info.entity.rigidbody.IsPlatform) {
					transform.position -= info.interval * info.axis;
					BodyToPlatform(entity, info.entity, info.axis);
				} else if(body.IsPlatform && !info.entity.rigidbody.IsPlatform) {
					info.entity.transform.position += info.interval * info.axis;
					BodyToPlatform(info.entity, entity, info.axis);
				} else {
					transform.position -= info.interval * info.axis;
					BodyToBody(entity, info.entity);
				}
			}
		}

		internal void BodyToBody(Entity entity, Entity otherEntity) {
			RigidBody body = entity.rigidbody;
			RigidBody otherBody = otherEntity.rigidbody;

			float ma = body.mass;
			float mb = otherBody.mass;
			Vector2 ua = body.velocity;
			Vector2 ub = otherBody.velocity;
			float Cr = body.bounciness * otherBody.bounciness * (1 - body.friction * otherBody.friction);
			Vector2 va = Cr * mb * (ub - ua) + ma * ua + mb * ub;
			va = va / (ma + mb);
			Vector2 vb = Cr * ma * (ua - ub) + ma * ua + mb * ub;
			vb = vb / (ma + mb);

			body.velocity = va + Vector2.Down * gravity * body.gravity * Time.deltaT;
			otherBody.velocity = vb + Vector2.Down * gravity * otherBody.gravity * Time.deltaT;
		}

		internal void BodyToPlatform(Entity entity, Entity platform, Vector2 axis) {
			RigidBody body = entity.rigidbody;
			Transform transform = entity.transform;
			RigidBody otherBody = platform.rigidbody;
			Collider otherCollider = platform.collider;

			float fallingvelocity = body.velocity.y;

			Vector2 perpendicularProjection = axis.Dot(body.velocity) * axis;
			Vector2 parallelProjection = body.velocity - perpendicularProjection;
			float bounceFactor = body.bounciness * otherBody.bounciness * (2 * body.mass / (body.mass + otherBody.mass));
			float frictionFactor = (1 - body.friction * otherBody.friction);

			Vector2 perpendicularReaction = bounceFactor * perpendicularProjection;
			Vector2 parallelReaction = parallelProjection * frictionFactor;
			body.velocity = parallelReaction - perpendicularReaction;
			
			if(fallingvelocity <= 0)
				body.velocity += Vector2.Down * gravity * body.gravity * Time.deltaT;
			
			// colliding with a vertical wall
			if(Math.Abs(axis.x) > 0.995f)
				body.velocity.y = fallingvelocity * frictionFactor;
		}
	}
}

