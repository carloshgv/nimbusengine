﻿using System;
using System.Collections.Generic;

namespace Nimbus {
	[Requires(typeof(Script))]
	public class ScriptSystem : Nimbus.System {
		public override void Init() {
			foreach(Entity entity in targets)
				foreach(Script component in entity.GetComponents<Script>())
					if(!component.Disabled) component.script.Init();
		}

		public override void PreUpdate(Entity entity) {
			foreach(Script component in entity.GetComponents<Script>())
				if(!component.Disabled) component.script.PreUpdate();
		}

		public override void PreFixedUpdate(Entity entity) {
			foreach(Script component in entity.GetComponents<Script>())
				if(!component.Disabled) component.script.PreFixedUpdate();
		}

		public override void FixedUpdate(Entity entity) {
			foreach(Script component in entity.GetComponents<Script>())
				if(!component.Disabled) component.script.FixedUpdate();
		}

		public override void Update(Entity entity) {
			foreach(Script component in entity.GetComponents<Script>())
				if(!component.Disabled) component.script.Update();
		}

		public override void LateUpdate(Entity entity) {
			foreach(Script component in entity.GetComponents<Script>())
				if(!component.Disabled) component.script.LateUpdate();
		}

		public override void Render(Entity entity) {
			foreach(Script component in entity.GetComponents<Script>())
				if(!component.Disabled) component.script.Render();
		}

		public override void LateRender(Entity entity) {
			foreach(Script component in entity.GetComponents<Script>())
				if(!component.Disabled) component.script.LateRender();
		}	
	}
}

