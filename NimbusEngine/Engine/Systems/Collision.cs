﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections;
using System.Diagnostics;

namespace Nimbus {
	[Requires(typeof(Transform), typeof(Collider))]
	public class Collision : System {
		List<Entity> collidables;
		Dictionary<Collider, Dictionary<Collider, OverlapInfo>> collisions;

		internal QuadTree tree;

		public override void Init() {
			collisions = new Dictionary<Collider, Dictionary<Collider, OverlapInfo>>();
		}

		public override void FramePrefix() {
			collidables = owner.GetEntitiesWithComponent<Collider>();
			collisions.Clear();

			float minx = float.MaxValue,
			miny = float.MaxValue,
			maxx = float.MinValue,
			maxy = float.MinValue;

			for(int i = 0; i < collidables.Count; i++) {
				Transform transform = collidables[i].transform;
				foreach(Collider collider in collidables[i].colliders) {
					collider.hitbox = collider.initialHitbox.Transform(transform) as ICollidable;
					if(collider.boundingBox.x1 < minx)
						minx = collider.boundingBox.x1;
					if(collider.boundingBox.x2 > maxx)
						maxx = collider.boundingBox.x2;
					if(collider.boundingBox.y1 < miny)
						miny = collider.boundingBox.y1;
					if(collider.boundingBox.y2 > maxy)
						maxy = collider.boundingBox.y2;

					collisions[collider] = new Dictionary<Collider, OverlapInfo>();
				}
			}

			tree = new QuadTree(new Rectangle(minx, miny, maxx - minx, maxy - miny));
			for(int i = 0; i < collidables.Count; i++)
				foreach(Collider collider in collidables[i].colliders)
					tree.Insert(collider);
		}

		public override void PreUpdate(Entity entity) {
			foreach(Collider collider in entity.colliders)
				collider.collisions = collisions;

			foreach(Collider collider in entity.colliders) {
				if(collider.Disabled)
					continue;
				foreach(var other in tree.Query(collider.boundingBox).Where(possible => entity.layerMask & possible.entity.layerMask && entity.id < possible.entity.id && !possible.Disabled)) {
					OverlapInfo info = collider.hitbox.Overlaps(other.hitbox);
					if(info) {
						collisions[collider][other] = info;
						collisions[other][collider] = info;
					}
				}
			}
		}

		public override void LateUpdate(Entity entity) {
			foreach(Collider collider in entity.colliders)
				collider.collisions = null;
		}

		public override void RenderDebug(Entity entity) {
			for(int i = 0; i < entity.colliders.Count; i++) {
				owner.MainWindow.SetColor((byte)(80 * (i%3)), 100, 0, 200);
				if(entity.colliders[i].hitbox.GetType() == typeof(Rectangle))
					owner.MainWindow.Fill((Rectangle)entity.colliders[i].hitbox);
				if(entity.colliders[i].hitbox.GetType() == typeof(Polygon))
					owner.MainWindow.Fill(entity.colliders[i].hitbox as Polygon);
			}
		}
	}

	internal class QuadTree {
		const int MAX_NODES = 20;
		List<Collider> containedColliders;
		Rectangle range;
		QuadTree nw, ne, sw, se;

		internal QuadTree(Rectangle range) {
			this.range = range;
			containedColliders = new List<Collider>(100);
		}

		internal bool Insert(Collider collider) {
			if(range.Contains(collider.boundingBox)) {
				if(nw == null && containedColliders.Count < MAX_NODES) {
					containedColliders.Add(collider);
					return true;
				} else {
					if(nw == null)
						SubDivide();
					if(nw.Insert(collider))
						return true;
					if(ne.Insert(collider))
						return true;
					if(sw.Insert(collider))
						return true;
					if(se.Insert(collider))
						return true;
					containedColliders.Add(collider);
					return true;
				}
			}
			return false;
		}

		internal List<Collider> Query(Rectangle queryRect) {
			if(queryRect.Overlaps(range)) {
				List<Collider> queryResult = new List<Collider>(containedColliders);
				if(nw != null) {
					queryResult.AddRange(nw.Query(queryRect));
					queryResult.AddRange(ne.Query(queryRect));
					queryResult.AddRange(sw.Query(queryRect));
					queryResult.AddRange(se.Query(queryRect));
				}
				return queryResult;
			}
			return new List<Collider>();
		}

		void SubDivide() {
			Rectangle newRange = range / 2;
			nw = new QuadTree(newRange);
			ne = new QuadTree(newRange + new Vector2(newRange.w, 0));
			sw = new QuadTree(newRange + new Vector2(0, newRange.h));
			se = new QuadTree(newRange + new Vector2(newRange.w, newRange.h));

			for(int i = 0; i < containedColliders.Count; i++) {
				bool moved = nw.Insert(containedColliders[i]);
				moved = ne.Insert(containedColliders[i]) || moved;
				moved = sw.Insert(containedColliders[i]) || moved;
				moved = se.Insert(containedColliders[i]) || moved;
				if(moved) {
					containedColliders.RemoveAt(i);
					i--;
				}
			}
		}
	}
}

