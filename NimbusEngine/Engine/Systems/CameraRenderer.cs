﻿using System;
using Nimbus.Assets;

namespace Nimbus {
	[Requires(typeof(Camera))]
	public class CameraRenderer : Nimbus.System {
		readonly Backend.Window target;

		public CameraRenderer(Backend.Window target) {
			this.target = target;
		}

		public override void Render(Entity entity) {
			Camera camera = entity.GetComponent<Camera>();

			foreach(var background in camera.backgrounds) {
				Rectangle src = new Rectangle(entity.transform.position * target.pixelsPerUnit * background.parallaxCoef,
				                              target.Viewport.Size + Vector2.Identity);
				src.x = Math.Max(src.x, 0);
				src.x = Math.Min(src.x, background.texture.rect.w - target.Viewport.Size.x);
				src.y = Math.Max(src.y, 0);
				src.y = Math.Min(src.y, background.texture.rect.h - target.Viewport.Size.y);
				Rectangle dest = new Rectangle(entity.transform.position * target.pixelsPerUnit, target.Viewport.Size + Vector2.Identity);

				target.QueueDraw(background.zorder, background.texture,
				                 src,
				                 dest,
				                 0,
				                 Vector2.Zero,
				                 SDL2.SDL.SDL_RendererFlip.SDL_FLIP_NONE);
			}
		}
	}
}

