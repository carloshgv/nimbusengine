﻿using System;
using SDL2;
using TiledSharp;
using Nimbus.Assets;

namespace Nimbus {
	[Requires(typeof(TiledMap))]
	public class TiledMapRenderer : System {
		readonly Backend.Window target;

		public TiledMapRenderer(Backend.Window target) {
			this.target = target;
		}

		public override void Render(Entity entity) {
			TiledMap component = entity.GetComponent<TiledMap>();
			if(component.Disabled)
				return;
			TmxMap map = component.map;
			Tileset tileset = component.tileset;

			Rectangle src = new Rectangle(0, 0, map.TileWidth, map.TileHeight);
			Rectangle dst = new Rectangle(0, 0, map.TileWidth, map.TileHeight);
			int gid;

			for(int k = 0; k < map.Layers.Count; k++) {
				for(int i = 0; i < map.Width; i++) {
					for(int j = 0; j < map.Height; j++) {
						gid = map.Layers[k].Tiles[j * map.Width + i].Gid;
						if(gid > 0) {
							gid--;
							src.x = (int)tileset[gid].x;
							src.y = (int)tileset[gid].y;
							dst.x = i * map.TileWidth;
							dst.y = j * map.TileHeight;
							target.QueueDraw(component.zorders[k],
							                 component.GidToTexture(gid),
							                 src,
							                 dst,
							                 0,
							                 Vector2.Zero,
							                 SDL.SDL_RendererFlip.SDL_FLIP_NONE);
						}
					}
				}
			}
		}
	}
}

