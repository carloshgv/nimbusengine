﻿using System;

namespace Nimbus {
	[Requires(typeof(Transform), typeof(SpriteSheet))]
	public class SpriteSheetRenderer : System {
		readonly Backend.Window target;

		public SpriteSheetRenderer(Backend.Window target) {
			this.target = target;
		}

		public override void Render(Entity entity) {
			SpriteSheet sheet = entity.GetComponent<SpriteSheet>();
			if(sheet.Disabled)
				return;
			Transform transform = entity.transform;

			Rectangle src = sheet.descriptor.GetElement(sheet.activeElement).rect;
			Vector2 offset = sheet.descriptor.GetElement(sheet.activeElement).offset;
			Rectangle dest = new Rectangle(0, 0, src.w, src.h) * transform.scale + (transform.position - offset * transform.scale) * target.pixelsPerUnit;
			Vector2 center = offset * transform.scale * target.pixelsPerUnit;
			target.QueueDraw(transform.zorder, sheet.texture, src, dest, transform.rotation.Deg, center, transform.flip);
		}

		public override void RenderDebug(Entity e) {
			Transform t = e.transform;

			target.SetColor(0, 0, 0, 255);
			target.Draw(new Rectangle(t.position - new Vector2(3, 3), new Vector2(6, 6)) / target.pixelsPerUnit);
		}
	}
}

