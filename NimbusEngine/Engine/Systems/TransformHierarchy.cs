﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Nimbus {
	[Requires(typeof(Transform))]
	public class TransformHierarchy : Nimbus.System {
		public override void PreUpdate(Entity entity) {
			entity.transform.lastPosition = entity.transform.position;
			entity.transform.lastRotation = entity.transform.rotation;
			entity.transform.lastScale = entity.transform.scale;
		}

		public override void FrameSuffix() {
			Stack<Transform> stack = new Stack<Transform>();
			foreach(var t in Transform.WorldOrigin.descendents)
				stack.Push(t);

			while(stack.Count > 0) {
				Transform current = stack.Pop();
				foreach(var descendent in current.descendents) {
					descendent.position = (descendent.position - current.lastPosition) * (current.scale / current.lastScale) * (current.rotation - current.lastRotation) + current.position;
					descendent.rotation += current.rotation - current.lastRotation;
					descendent.scale *= current.scale / current.lastScale;
					stack.Push(descendent);
				}
			}
		}
	}
}

