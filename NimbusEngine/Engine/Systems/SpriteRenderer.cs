﻿using System;

namespace Nimbus {
	[Requires(typeof(Transform), typeof(Sprite))]
	public class SpriteRenderer : System {
		readonly Backend.Window target;

		public SpriteRenderer(Backend.Window target) {
			this.target = target;
		}

		public override void Render(Entity entity) {
			Sprite sprite = entity.GetComponent<Sprite>();
			if(sprite.Disabled)
				return;
			Transform transform = entity.transform;

			Rectangle src = sprite.texture.rect;
			Rectangle dest = new Rectangle(0, 0, src.w, src.h) * transform.scale + (transform.position - sprite.offset * transform.scale) * target.pixelsPerUnit;
			Vector2 center = sprite.offset * transform.scale * target.pixelsPerUnit;
			target.QueueDraw(transform.zorder, sprite.texture, dest, transform.rotation.Deg, center, SDL2.SDL.SDL_RendererFlip.SDL_FLIP_NONE);
		}
	}
}

