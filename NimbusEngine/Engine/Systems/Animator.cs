﻿using System;

namespace Nimbus {
	[Requires(typeof(SpriteSheet), typeof(Animation))]
	public class Animator : System {
		public override void Update(Entity entity) {
			Animation animation = entity.animation;
			if(animation.Disabled)
				return;
			SpriteSheet sheet = entity.GetComponent<SpriteSheet>();

			animation.elapsed += Time.deltaT;
			if(animation.elapsed > animation.timeBetweenFrames) {
				animation.currentFrame = (animation.currentFrame + ((int)(animation.elapsed / animation.timeBetweenFrames)));
				if(animation.currentFrame >= animation.states[animation.currentState].Length) {
					if(animation.onAnimationEnd != null)
						animation.onAnimationEnd();
					animation.onAnimationEnd = null;
					animation.currentFrame = animation.currentFrame % animation.states[animation.currentState].Length;
				}
				sheet.activeElement = animation.states[animation.currentState][animation.currentFrame];
				animation.elapsed -= animation.timeBetweenFrames * (int)(animation.elapsed / animation.timeBetweenFrames);
			}
		}
	}
}

