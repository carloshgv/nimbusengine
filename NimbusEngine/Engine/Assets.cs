﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Xml;
using SDL2;

namespace Nimbus {
	namespace Assets {
		public class Texture {
			public readonly IntPtr data;
			public readonly Rectangle rect;

			public Texture(IntPtr data, Rectangle rect) {
				this.data = data;
				this.rect = rect;
			}
		}

		public class SpriteSheetDescriptor {
			private readonly List<SpriteSheet.Element> elements;

			public SpriteSheetDescriptor(string file) {
				XmlReader reader = XmlReader.Create(file);
				elements = new List<SpriteSheet.Element>();
				while(reader.Read()) {
					if(reader.NodeType == XmlNodeType.Element && reader.Name == "sprite") {
						SpriteSheet.Element el = new SpriteSheet.Element();
						el.rect = new Rectangle(
							int.Parse(reader.GetAttribute("x")),
							int.Parse(reader.GetAttribute("y")),
							int.Parse(reader.GetAttribute("w")),
							int.Parse(reader.GetAttribute("h"))
						);
						el.offset = new Vector2((float)Math.Floor(el.rect.w * float.Parse(reader.GetAttribute("pX"), CultureInfo.InvariantCulture)),
						                         (float)Math.Floor(el.rect.h * float.Parse(reader.GetAttribute("pY"), CultureInfo.InvariantCulture)));

						elements.Add(el);
					}
				}
			}

			public SpriteSheet.Element GetElement(int el) {
				return elements[el];
			}
		}

		public class Audio {
			public readonly IntPtr data;

			public Audio(string file) {
				data = SDL_mixer.Mix_LoadWAV(file);
				if(data == IntPtr.Zero)
					Console.WriteLine("error loading " + file + ": " + SDL.SDL_GetError());
			}
		}

		public class Font {
			internal int baseSize;

			protected Font(int size) {
				baseSize = size;
			}
		}

		public class TrueTypeFont : Font {
			public readonly IntPtr data;

			public TrueTypeFont(string file, int size) : base(size) {
				data = SDL_ttf.TTF_OpenFont(file, size);
				if(data == IntPtr.Zero)
					Console.WriteLine("error loading " + file + ": " + SDL.SDL_GetError());
			}
		}

		public class BitmapFont : Font {
			public BitmapFont(int size) : base(size) {
			}
		}
	}
}

