﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Nimbus {
	public static class Extensions {
		public static RayHit Raycast(this Entity entity, Vector2 origin, Vector2 direction, LayerMask? inputMask = null) {
			LayerMask mask = inputMask ?? LayerMask.All;
			Segment ray = new Segment(entity.transform.position + origin, entity.transform.position + origin + direction);
			Rectangle consideredArea = new Rectangle(entity.transform.position + origin, direction);
			var intersections = new List<Tuple<Collider, Vector2, Vector2>>();
			var found = entity.owner.collisionSystem.tree.Query(consideredArea).Where(candidate => candidate.entity.layerMask & mask && candidate.entity != entity && candidate.hitbox.Overlaps(consideredArea));
			foreach(var f in found) {
				foreach(Segment edge in f.hitbox.Edges) {
					Vector2? intersectionPoint = MathUtil.Intersection(ray, edge);
					if(intersectionPoint != null)
						intersections.Add(Tuple.Create(f, (Vector2)intersectionPoint, (edge.end - edge.start).UnitNormal));
				}
			}
			if(intersections.Count > 0) {
				intersections.Sort((a, b) => {
					return (int)(origin.SqrDistance(a.Item2) - origin.SqrDistance(b.Item2));
				});
				return new RayHit(intersections.Select(i => i.Item1).ToArray(), 
				                  intersections.Select(i => i.Item2).ToArray(), 
				                  intersections.Select(i => i.Item3).ToArray());
			}
			return null;
		}
	}
}
