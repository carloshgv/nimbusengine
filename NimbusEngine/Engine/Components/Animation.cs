﻿using System;
using System.Collections.Generic;
using SDL2;

namespace Nimbus {
	public struct AnimState {
		public string id;
		public int[] frames;
	}

	[Unique]
	public class Animation : Component {
		public delegate void OnAnimationEnd();
		internal Dictionary<string, int[]> states;
		internal float elapsed = 0;
		internal string previousState;
		internal string currentState;
		internal int currentFrame;
		internal float timeBetweenFrames;
		internal OnAnimationEnd onAnimationEnd;

		public float Fps {
			get { return 1f / timeBetweenFrames; }
			set { timeBetweenFrames = 1f / value; }
		}

		public Animation(float fps = 24) {
			timeBetweenFrames = 1f / fps;
			elapsed = timeBetweenFrames + 0.01f;
			states = new Dictionary<string, int[]>();
		}

		public Animation(float fps, params AnimState[] states) {
			timeBetweenFrames = 1f / fps;
			elapsed = timeBetweenFrames + 0.01f;
			this.states = new Dictionary<string, int[]>();
			foreach(var state in states)
				AddState(state);
		}

		public void AddState(string id, params int[] frames) {
			states[id] = frames;
			if(currentState == null)
				currentState = id;
		}

		public void AddState(AnimState state) {
			states[state.id] = state.frames;
			if(currentState == null)
				currentState = state.id;
		}

		public void SetState(string id, OnAnimationEnd callback = null) {
			if (states.ContainsKey (id) && id != currentState) {
				previousState = currentState;
				currentState = id;
				currentFrame = 0;
			}
			onAnimationEnd = callback;
		}

		public string GetState() {
			return currentState;
		}

		public string GetPreviousState() {
			return previousState;
		}
	}
}

