﻿using System;
using System.Collections.Generic;
using Nimbus.Assets;

namespace Nimbus {
	[Unique]
	public class Camera : Component {
		public struct Background {
			public Texture texture;
			public Vector2 parallaxCoef;
			public int zorder;
		}

		public Color backgroundColor;
		public List<Background> backgrounds;

		public Camera(Color? bgcolor = null) {
			this.backgroundColor = bgcolor ?? Color.Black;
			this.backgrounds = new List<Background>();
		}

		public void AddBackgroundLayer(Texture texture, Vector2 parallaxCoef, int zorder = int.MaxValue) {
			this.backgrounds.Add(new Background() { texture = texture, parallaxCoef = parallaxCoef, zorder = zorder });
		}
	}
}

