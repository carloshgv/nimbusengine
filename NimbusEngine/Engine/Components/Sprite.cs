﻿using System;
using Nimbus;
using Nimbus.Assets;

namespace Nimbus {
	[Unique]
	public class Sprite : Component {
		public Texture texture;
		public Vector2 offset;

		public Sprite(Texture texture) {
			this.texture = texture;
			this.offset = Vector2.CenterCenter * texture.rect.Size;
		}

		public Sprite(Texture texture, Vector2 offset) {
			this.texture = texture;
			this.offset = offset * texture.rect.Size;
		}
	}
}

