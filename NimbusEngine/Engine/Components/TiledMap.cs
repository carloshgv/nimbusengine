﻿using System;
using System.Collections.Generic;
using Nimbus;
using Nimbus.Assets;
using TiledSharp;

namespace Nimbus {
	[Unique]
	public class Tileset {
		private List<Vector2> tiles;

		internal Tileset(TmxMap map) {
			tiles = new List<Vector2>();
			for(int k = 0; k < map.Tilesets.Count; k++)
				for(int y = 0; y < map.Tilesets[k].Image.Height; y += map.Tilesets[k].TileHeight + map.Tilesets[k].Spacing)
					for(int x = 0; x < map.Tilesets[k].Image.Width; x += map.Tilesets[k].TileWidth + map.Tilesets[k].Spacing)
						tiles.Add(new Vector2(x, y));
		}

		public Vector2 this[int gid] {
			get {
				return tiles[gid];
			}
		}
	}

	public class TiledMap : Component {
		public readonly Tileset tileset;
		public readonly Texture[] textures;
		public readonly TmxMap map;
		public readonly Vector2 size;
		public readonly int[] zorders;

		int[] gidToTexture;

		public TiledMap(Texture[] textures, TmxMap map) {
			this.map = map;
			this.textures = textures;
			gidToTexture = new int[textures.Length];
			for(int i = 0; i < textures.Length; i++)
				gidToTexture[i] = map.Tilesets[i].FirstGid - 1;
			zorders = new int[map.Layers.Count];
			for(int i = 0; i < map.Layers.Count; i++) {
				string zorderString;
				if(!map.Layers[i].Properties.TryGetValue("zorder", out zorderString) || !int.TryParse(zorderString, out zorders[i]))
					zorders[i] = 100 + i;
			}
			tileset = new Tileset(map);
			size = new Vector2(map.Width * map.TileWidth, map.Height * map.TileHeight);
		}

		public int GidToTextureNumber(int gid) {
			for(int i = gidToTexture.Length - 1; i >= 0; i--)
				if(gid >= gidToTexture[i])
					return i;
			throw new ApplicationException("tile number " + gid + " not found");
		}

		public Texture GidToTexture(int gid) {
			return textures[GidToTextureNumber(gid)];
		}
	}
}

