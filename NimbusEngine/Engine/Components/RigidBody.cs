﻿using System;

namespace Nimbus {
	[Unique]
	public class RigidBody : Component {
		public float gravity;
		public float bounciness;
		public float friction;
		public float drag;
		public float mass;

		public bool IsPlatform;

		public Vector2 velocity;

		public bool MovingX {
			get { return Math.Abs(velocity.x) * Time.deltaT > 0f; }
		}

		public bool MovingY {
			get { return Math.Abs(velocity.y) * Time.deltaT > 0f; }
		}

		public bool Moving {
			get { return MovingX || MovingY; }
		}

		public RigidBody(float gravity = 1f, float bounciness = 0f,
		                 float friction = 0f, float drag = 0f, float mass = 1f, bool isPlatform = false) {
			this.gravity = gravity;
			this.bounciness = bounciness;
			this.friction = friction;
			this.drag = drag;
			this.mass = mass;
			this.IsPlatform = isPlatform;
		}

		public void ApplyForce(Vector2 force) {
			velocity += force * Time.deltaT;
		}
	}
}

