﻿using System;
using Nimbus.Assets;
using SDL2;

namespace Nimbus {
	public class AudioSource : Component {
		public delegate void PlayRequest(AudioSource source);
		public delegate float DecayFunction(float distance, float radius);
		public event PlayRequest OnPlayRequest;

		public Audio audio;
		public bool loop;
		public float volume;
		public float radius;
		public DecayFunction Decay;

		internal int channel;
		internal bool playOnStart;

		public bool IsPlaying { get { return channel >= 0; } }

		public AudioSource(Audio audio, float volume = 1f, bool loop = false, bool playOnStart = false, float radius = 10f, DecayFunction decay = null) {
			this.audio = audio;
			this.volume = volume;
			this.loop = loop;
			channel = -1;
			this.playOnStart = playOnStart;
			this.radius = radius;
			this.Decay = decay ?? DecayCurves.None;
		}

		public void Play() {
			OnPlayRequest(this);
		}

		public void Stop() {
			Backend.Sound.Stop(channel);
		}

		public static class DecayCurves {
			public static float None(float distance, float radius) {
				if(distance < radius)
					return 1f;
				else
					return 0;
			}

			public static float Linear(float distance, float radius) {
				if(distance < radius)
					return 1 - distance / radius;
				else
					return 0;
			}

			public static float Inverse(float distance, float radius) {
				if(distance == 0)
					return 1;
				else
					return 1 / distance;
			}
		}
	}
}

