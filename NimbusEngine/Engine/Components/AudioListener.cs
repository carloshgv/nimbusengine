﻿using System;

namespace Nimbus {
	public class AudioListener : Component {
		public float threshold;
		public float amplification;

		public AudioListener(float threshold = 0f, float amplification = 1f) {
			this.threshold = threshold;
			this.amplification = amplification;
		}
	}
}

