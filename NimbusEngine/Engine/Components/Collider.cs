﻿using System;
using System.Runtime.CompilerServices;
using System.Collections.Generic;
using System.Linq;

namespace Nimbus {
	public class Collider : Component {
		internal Dictionary<Collider, Dictionary<Collider, OverlapInfo>> collisions;
		internal ICollidable initialHitbox;

		public ICollidable hitbox;
		public string colliderTag;

		public string tag {
			get { return colliderTag ?? entity.tag; }
			set { colliderTag = value; }
		}

		public Rectangle boundingBox {
			get {
				return hitbox.BoundingBox;
			}
		}

		public List<Collider> CollidingWith {
			get { return collisions[this].Select(c => c.Key).ToList(); }
		}

		public List<CollisionInfo> Collisions {
			get { return collisions[this].Select(c => new CollisionInfo(c.Key, c.Value.separationAxis, c.Value.interval)).ToList(); }
		}

		public Collider(string tag, ICollidable hitbox) {
			this.initialHitbox = hitbox;
			this.colliderTag = tag;
		}

		public Collider(ICollidable hitbox) : this(null, hitbox) {
		}

		public void ChangeHitbox(ICollidable newHitbox) {
			this.initialHitbox = newHitbox;
		}
	}
}
