﻿using System;
using System.Collections.Generic;
using SDL2;

namespace Nimbus {
	[Unique]
	public class Transform : Component {
		internal static Transform WorldOrigin;

		public Vector2 position;
		public Vector2 scale;
		public Rotation rotation;
		public int zorder;
		public Transform parentTransform;
		public List<Transform> descendents;

		public Transform parent {
			get {
				return parentTransform;
			}
			set {
				parentTransform.descendents.Remove(this);
				value.descendents.Add(this);
				parentTransform = value;
			}
		}

		public Vector2 localPosition { get { return ((position - parentTransform.position) / parentTransform.scale) * (-parentTransform.rotation); } }

		public Rotation localRotation { get { return rotation - parentTransform.rotation; } }

		public Vector2 localScale { get { return scale / parentTransform.scale; } }

		public bool hasMoved { get { return position != lastPosition || scale != lastScale || rotation != lastRotation; } }

		internal SDL.SDL_RendererFlip flip = SDL.SDL_RendererFlip.SDL_FLIP_NONE;
		internal Vector2 lastPosition;
		internal Rotation lastRotation;
		internal Vector2 lastScale;

		static Transform() {
			WorldOrigin = new Transform(true);
		}

		private Transform(bool isWorldOriginConstructor) {
			this.position = this.lastPosition = Vector2.Zero;
			this.scale = this.lastScale = Vector2.Identity;
			this.rotation = this.lastRotation = Rotation.Zero;
			this.zorder = 0;
			this.parentTransform = null;
			this.descendents = new List<Transform>();
		}

		public Transform(Vector2? position = null, Vector2? scale = null, Rotation? rotation = null, int zorder = 0) {
			this.position = position ?? Vector2.Zero;
			this.scale = scale ?? Vector2.Identity;
			this.rotation = rotation ?? Rotation.Zero;
			this.zorder = zorder;
			this.descendents = new List<Transform>();

			this.lastPosition = this.position;
			this.lastRotation = this.rotation;
			this.lastScale = this.scale;
		}

		public override void OnAddition() {
			parentTransform = WorldOrigin;
			WorldOrigin.descendents.Add(this);
		}

		public override void OnRemoval() {
			parentTransform.descendents.Remove(this);
			parentTransform = null;
		}

		public void FlipH(bool f = true) {
			if(f)
				flip = flip | SDL.SDL_RendererFlip.SDL_FLIP_HORIZONTAL;
			else
				flip = flip & ~SDL.SDL_RendererFlip.SDL_FLIP_HORIZONTAL;
		}

		public void FlipV(bool f = true) {
			if(f)
				flip = flip | SDL.SDL_RendererFlip.SDL_FLIP_VERTICAL;
			else
				flip = flip & ~SDL.SDL_RendererFlip.SDL_FLIP_VERTICAL;
		}

		public static Rectangle operator *(Rectangle r, Transform t) {
			if(Math.Abs(t.rotation.Rad) < float.Epsilon) {
				return new Rectangle(
					r.x * t.scale.x + t.position.x,
					r.y * t.scale.y + t.position.y,
					r.w * t.scale.x,
					r.h * t.scale.y
				);
			} else
				return ((new Polygon(r)) * t.scale * t.rotation).BoundingBox + t.position;
		}

		public static Vector2 operator *(Vector2 v, Transform t) {
			return new Vector2(
				v.x * t.scale.x + t.position.x,
				v.y * t.scale.y + t.position.y
			);
		}

		public static Polygon operator *(Polygon poly, Transform t) {
			return new Polygon(Array.ConvertAll(poly.points, point => point * t.scale * t.rotation + t.position));
		}

		public bool IsSameAs(Transform other) {
			return position.Equals(other.position) && scale.Equals(other.scale) && rotation.Equals(other.rotation);
		}
	}
}

