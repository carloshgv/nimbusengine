﻿using System;
using System.Xml;
using System.Globalization;
using System.Collections.Generic;
using Nimbus;
using Nimbus.Assets;

namespace Nimbus {
	[Unique]
	public class SpriteSheet : Component {
		public struct Element {
			public Rectangle rect;
			public Vector2 offset;
		}

		public readonly Texture texture;
		public readonly SpriteSheetDescriptor descriptor;
		public int activeElement;

		public SpriteSheet(Texture texture, SpriteSheetDescriptor descriptor, int element = 0) {
			this.texture = texture;
			this.descriptor = descriptor;
			this.activeElement = element;
		}
	}
}

