﻿using System;
using Nimbus;

namespace Nimbus {
	public class Script : Component {
		internal Type scriptType;
		public NimbusScript script;

		public Script(Type type) {
			if(!type.IsSubclassOf(typeof(NimbusScript)))
				throw new ArgumentException("the type isn't a NimbusScript");
			scriptType = type;
		}

		public override void OnAddition() {
			script = Activator.CreateInstance(scriptType) as NimbusScript;
			script.engine = engine;
			script.entity = entity;
		}
	}
}

