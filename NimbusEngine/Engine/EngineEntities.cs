﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nimbus.Backend;
using SDL2;
using System.Diagnostics;

namespace Nimbus {
	public partial class Engine {
		public delegate Entity EntityBuilder();

		Dictionary<string, EntityBuilder> entityTemplates;

		internal class EntityProxy {
			string tag;
			Engine engine;

			internal EntityProxy(string tag, Engine engine) {
				this.tag = tag;
				this.engine = engine;
			}

			internal Entity Resolve() {
				return engine.FindEntityByTag(tag);
			}
		}

		public Entity CreateEntity(params Component[] comps) {
			int availableId = lastId++;
			var e = new Entity(availableId, this);
			e.layerMask = LayerMask.All;
			entities.Add(e);
			components.Add(new Dictionary<Type, List<Component>>());
			foreach(var c in comps)
				AddComponent(e, c);

			return e;
		}

		public Entity CreateEntity(string tag, params Component[] comps) {
			Entity e = CreateEntity(comps);
			e.tag = tag;
			e.layerMask = LayerMask.All;
			return e;
		}

		public Entity CreateEntity(LayerMask mask, params Component[] comps) {
			Entity e = CreateEntity(comps);
			e.layerMask = mask;
			return e;
		}

		public Entity CreateEntity(string tag, LayerMask mask, params Component[] comps) {
			Entity e = CreateEntity(comps);
			e.tag = tag;
			e.layerMask = mask;
			return e;
		}

		public Entity CreateEntityFromTemplate(string id) {
			return entityTemplates[id]();
		}

		public void RegisterTemplate(string id, EntityBuilder builder) {
			entityTemplates[id] = builder;
		}

		public void DestroyEntity(Entity e) {
			markedForDestroy.Add(e);
		}

		public List<Entity> FindEntitiesByTag(string tag) {
			return entities.FindAll(e => e.tag == tag).ToList();
		}

		public Entity FindEntityByTag(string tag) {
			return entities.Find(e => e.tag == tag);
		}

		public List<Entity> GetEntitiesWithComponent<T>() {
			return entities.FindAll(e => components[e.id].ContainsKey(typeof(T)));
		}

		void PurgeEntities() {
			foreach(Entity e in markedForDestroy) {
				foreach(var sys in systemTargets)
					sys.Value.Remove(e);
				foreach(var pair in components[e.id])
					foreach(var component in pair.Value) {
						component.OnRemoval();
						component.entity = null;
					}
				components[e.id].Clear();
				changedEntities.Remove(e);
				entities.Remove(e);
			}
			markedForDestroy.Clear();
		}
		//**********************************************************************
		internal Component GetComponent(int id, Type type) {
			List<Component> ret;
			components[id].TryGetValue(type, out ret);
			return (ret != null) ? ret[0] : null;
		}

		internal List<Component> GetComponents(int id, Type type) {
			List<Component> ret;
			components[id].TryGetValue(type, out ret);
			return ret;
		}

		internal Component AddComponent(Entity e, Component comp) {
			if(!components[e.id].ContainsKey(comp.GetType()))
				components[e.id][comp.GetType()] = new List<Component>();
			else if(comp.GetType().IsDefined(typeof(UniqueAttribute), false))
				throw new ArgumentException("just one component of type " + comp.GetType() + " is allowed");

			components[e.id][comp.GetType()].Add(comp);
			comp.entity = e;
			comp.engine = this;
			comp.OnAddition();
			changedEntities.Add (e);
			return comp;
		}

		internal void RemoveComponent(Entity e, Component comp) {
			components[e.id][comp.GetType()].Remove(comp);
			if(components[e.id][comp.GetType()].Count == 0)
				components[e.id].Remove(comp.GetType());
			comp.OnRemoval();
			comp.entity = null;
			comp.engine = null;
			changedEntities.Add (e);
		}
	}
}

