﻿using System;
using System.Collections.Generic;

namespace Nimbus {
	[AttributeUsage(AttributeTargets.Class)]
	public class RequiresAttribute : Attribute {
		public HashSet<Type> requires;

		public RequiresAttribute(params Type[] components) {
			requires = new HashSet<Type>();
			foreach(var c in components)
				requires.Add(c);
		}
	}

	[AttributeUsage(AttributeTargets.Class)]
	public class UniqueAttribute : Attribute {
	}
}

