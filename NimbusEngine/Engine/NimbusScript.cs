﻿using System;
using System.Collections.Generic;

namespace Nimbus {
	public abstract class NimbusScript {
		public Entity entity;
		protected internal Engine engine;

		public NimbusScript() {
		}

		public virtual void Init() {
		}

		public virtual void PreUpdate() {
		}

		public virtual void PreFixedUpdate() {
		}

		public virtual void FixedUpdate() {
		}

		public virtual void Update() {
		}

		public virtual void LateUpdate() {
		}

		public virtual void Render() {
		}

		public virtual void LateRender() {
		}

		protected T GetSystem<T>() {
			return engine.GetSystem<T>();
		}

		protected Entity CreateEntity(params Component[] comps) {
			return engine.CreateEntity(comps);
			;
		}

		protected Entity CreateEntity(string tag, params Component[] comps) {
			return engine.CreateEntity(tag, comps);
		}

		protected Entity CreateEntity(LayerMask mask, params Component[] comps) {
			return engine.CreateEntity(mask, comps);
		}

		protected Entity CreateEntity(string tag, LayerMask mask, params Component[] comps) {
			return engine.CreateEntity(tag, mask, comps);
		}

		protected Entity CreateEntityFromTemplate(string id) {
			return engine.CreateEntityFromTemplate(id);
		}

		protected void DestroyEntity(Entity e) {
			engine.DestroyEntity(e);
		}

		protected List<Entity> FindEntitiesByTag(string tag) {
			return engine.FindEntitiesByTag(tag);
		}

		protected Entity FindEntityByTag(string tag) {
			return engine.FindEntityByTag(tag);
		}

		protected List<Entity> GetEntitiesWithComponent<T>() {
			return engine.GetEntitiesWithComponent<T>();
		}

		protected  void LoadLevel(string level) {
			engine.LoadLevel(level);
		}

		protected  void ReplayLevel() {
			engine.ReplayLevel();
		}

		protected Backend.Window MainWindow { get { return engine.MainWindow; } }

		protected AssetManager AssetManager { get { return engine.AssetManager; } }

		protected T GetComponent<T>() where T : Component {
			return (T)Convert.ChangeType(engine.GetComponent(entity.id, typeof(T)), typeof(T));
		}

		protected List<T> GetComponents<T>() where T : Component {
			var components = engine.GetComponents(entity.id, typeof(T));
			if(components == null)
				return null;
			else
				return engine.GetComponents(entity.id, typeof(T)).ConvertAll(c => (T)Convert.ChangeType(c, typeof(T)));
		}

		protected RigidBody rigidbody { get { return entity.rigidbody; } }

		protected Transform transform { get { return entity.transform; } }

		protected Animation animation { get { return entity.animation; } }

		protected Collider collider { get { return entity.collider; } }

		protected List<Animation> animations { get { return entity.animations; } }

		protected List<Collider> colliders { get { return entity.colliders; } }

		protected List<Collider> CollidingWith { get { return collider.CollidingWith; } }

		protected RayHit Raycast(Vector2 origin, Vector2 direction, LayerMask? inputMask = null) {
			return entity.Raycast(origin, direction, inputMask);
		}
	}
}

