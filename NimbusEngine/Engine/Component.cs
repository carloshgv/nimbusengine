﻿using System;

namespace Nimbus {
	public class Component {
		public Entity entity;
		public bool Disabled;

		internal Engine engine;

		public virtual void OnAddition() { }
		public virtual void OnRemoval() { }
	}
}

