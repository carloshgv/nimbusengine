﻿using System;
using System.Collections.Generic;
using Nimbus.Assets;
using Nimbus.Backend;
using System.IO;
using TiledSharp;

namespace Nimbus {
	public class AssetManager {
		public readonly Window window;
		public readonly string assetsRoot;

		Dictionary<string, object> assetCache;

		public AssetManager(Window window) {
			assetsRoot = "Assets";
			assetCache = new Dictionary<string, object>();
			this.window = window;
		}

		private string GetPath(string id) {
			List<string> path = new List<string> { assetsRoot };
			List<string> splitId = new List<string>(id.Split(new Char[]{ '/', Path.DirectorySeparatorChar }));
			if(splitId[0] == assetsRoot)
				splitId.RemoveAt(0);
			path.AddRange(splitId);
			return Path.Combine(path.ToArray());
		}

		public Texture LoadTexture(string id) {
			if(assetCache.ContainsKey(id))
				return assetCache[id] as Texture;

			Texture tex = window.LoadTexture(GetPath(id)) as Texture;
			assetCache[id] = tex;
			return tex;
		}

		public SpriteSheetDescriptor LoadSheetDescriptor(string id) {
			if(assetCache.ContainsKey(id))
				return assetCache[id] as SpriteSheetDescriptor;

			SpriteSheetDescriptor desc = new SpriteSheetDescriptor(GetPath(id));
			assetCache[id] = desc;
			return desc;
		}

		public TmxMap LoadTiledMap(string id) {
			if(assetCache.ContainsKey(id))
				return assetCache[id] as TmxMap;

			TmxMap map = new TmxMap(GetPath(id));
			assetCache[id] = map;
			return map;
		}

		public Audio LoadAudio(string id) {
			if(assetCache.ContainsKey(id))
				return assetCache[id] as Audio;

			Audio audio = new Audio(GetPath(id));
			assetCache[id] = audio;
			return audio;
		}

		public TrueTypeFont LoadTrueTypeFont(string id, int size) {
			if(assetCache.ContainsKey(id + size))
				return assetCache[id + size] as TrueTypeFont;

			TrueTypeFont font = new TrueTypeFont(GetPath(id), size);
			if(!assetCache.ContainsKey(id))
				assetCache[id] = font;
			assetCache[id + size] = font;
			return font;
		}

		public TrueTypeFont LoadTrueTypeFont(string id) {
			if(assetCache.ContainsKey(id))
				return assetCache[id] as TrueTypeFont;
			else
				return null;
		}
	}
}

