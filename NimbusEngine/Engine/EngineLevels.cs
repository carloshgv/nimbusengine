﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nimbus.Backend;
using SDL2;
using System.Diagnostics;

namespace Nimbus {
	public partial class Engine {
		internal delegate void LevelBuilder();

		internal Dictionary<string, LevelBuilder> levelBuilders;
		string currentLevel;

		public void LoadLevel(string level) {
			StartLoop = (object sender, EventArgs e) => {
				Reset();
				currentLevel = level;
				LevelBuilder builder;
				levelBuilders.TryGetValue(level, out builder);
				if(builder == null)
					throw new ArgumentException("level " + level + " does not exist");
				builder();
				UpdateSystemTargets();
				foreach(var sys in Phases.init)
					sys.Init();
				Time.InitTimer();
			};
		}

		public void ReplayLevel() {
			LoadLevel(currentLevel);
		}
	}
}

